//#include <analogWrite.h>

/*
 * IRremote: IRrecvDemo - demonstrates receiving IR codes with IRrecv
 * An IR detector/demodulator must be connected to the input RECV_PIN.
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 */

// 6 = 16743045
// 3 = 16734885
// * = 16728765
// 長按 = 4294967295
// # = 16732845
// 0 = 16728765

#include <IRremote.h>

int RECV_PIN = 15;
int MOSFET_RECV = 18;
int BOARD_LED = 2;

IRrecv irrecv(RECV_PIN);
decode_results results;

const int freq = 40;
const int fanChannel = 0;
const int resolution = 8;

int lastPress = -1;

int fanSpeed = 0;
int minFanSpeed = 125;
int maxFanSpeed = 255;

int incomingByte = 0;

void AddFanSpeed(int avalue){
    if(fanSpeed < minFanSpeed){
      fanSpeed = minFanSpeed;
    }
    else if(fanSpeed + avalue <= maxFanSpeed){
        fanSpeed += avalue;
      }
      ledcWrite(fanChannel, fanSpeed);
      Serial.println(fanSpeed);
}

void DecreaseFanSpeed(int dvalue){
  if(fanSpeed - dvalue >= minFanSpeed){
      fanSpeed -= dvalue;
      if(fanSpeed <= minFanSpeed){
        fanSpeed == minFanSpeed;
      }
    }
    ledcWrite(fanChannel, fanSpeed);
    Serial.println(fanSpeed + "fanSpeed");
}

void setup()
{
  Serial.begin(115200);
  fanSpeed = 0;
  
  ledcSetup(fanChannel, freq, resolution);
  ledcAttachPin(MOSFET_RECV, fanChannel);
  ledcWrite(fanChannel, fanSpeed);
  pinMode(MOSFET_RECV, OUTPUT);
  pinMode(BOARD_LED, OUTPUT);
  // In case the interrupt driver crashes on setup, give a clue
  // to the user what's going on.
  Serial.println("Enabling IRin");
  irrecv.enableIRIn(); // Start the receiver
  Serial.println("Enabled IRin");
  digitalWrite(BOARD_LED, LOW); 
}

void loop() {
    
  if(Serial.available() > 0){
      incomingByte = Serial.read();
      //Serial.println(incomingByte);
    }
  
  if(incomingByte == B01000100){
    fanSpeed = 255;
      lastPress = -1;
      ledcWrite(fanChannel, fanSpeed);
      digitalWrite(BOARD_LED, HIGH); 
  }
  else if(incomingByte == B01100100){
    fanSpeed = 0;
      lastPress = -1;
      ledcWrite(fanChannel, fanSpeed);
      digitalWrite(BOARD_LED, LOW); 
  }
  if (irrecv.decode(&results)) {
    //Serial.println(results.value);
    if(results.value == 16761405){ // Unity Intergration
      //incomingByte = B11000100;
      Serial.print(11000000);
    }
    else if(results.value == 16720605){
      Serial.print(10000000);
    }
    else if(results.value == 16728765){
      incomingByte = 0;
      fanSpeed = 0;
      lastPress = -1;
      ledcWrite(fanChannel, fanSpeed);
      Serial.print(11100000);
    }
    
    else{
      lastPress = -1;
    }
    //Serial.println(results.value);
    irrecv.resume(); // Receive the next 20
  }

  
  delay(30);
}
