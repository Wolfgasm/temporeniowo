﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Sound : ScriptableObject {
     
    

    public abstract void Play(AudioSource source);
    //public AudioSource audioSource;
    public AudioClip audio;

    private void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
}

