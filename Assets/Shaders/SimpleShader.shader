﻿Shader "Unlit/SimpleShader"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_Gloss("Gloss", Range(1,200)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag


            #include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			// mesh data : vertex pos, vertex normal, tangents, vertex colors
            struct VertexInput
            {
                float4 vertex : POSITION;
				float2 uv0 : TEXCOORD0;//
				float3 normal : NORMAL;
            };
			

            struct VertexOutput
            {
                float4 vertex : SV_POSITION;
				float2 uv0 : TEXCOORD0;//
				float3 normal : TEXCOORD1; // 我以為語意要打normal well 也可以
				float3 worldPos : TEXCOORD2; // ______________________________________這些參數被稱作"interporator"
            };

            //sampler2D _MainTex;
            //float4 _MainTex_ST;

			float4 _Color;

			VertexOutput vert (VertexInput v)
            {
				VertexOutput o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul (unity_ObjectToWorld, v.vertex); // 單純跟unity內建matrix相乘(unity_ObjectToWorld in this case)就能取得worldposition
				o.uv0 = v.uv0;//
				o.normal = v.normal;
                return o;
            }

			float _Gloss;

			//UNITY built in variable
			//"https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html"
            float4 frag (VertexOutput i) : SV_Target
            {
				//return float4(i.worldPos, 1);
				//return _Color;
				float2 uv = i.uv0;
				float3 normal = i.normal; // normal是-1~1 所以有的顏色不會顯示 要顯示就把他remap成0~1

				float3 normalI = normalize(i.normal); // normalize the interpolation for correct shading
				//remap
				//normal = normal * 0.5 + 0.5; // 寫 *0.5比寫/2好
				// dot products : 描述兩向量之間角度的差距 為 -1~1

				// lambert diffuse
				float3 sunDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 lightColor = _LightColor0.rgb;
				float lightFalloff = saturate( dot(normal, sunDir)); // 記得處理負數 
				lightFalloff = step(0.05, lightFalloff); //toony
				float3 diffuse = lightColor * lightFalloff;
				// ambient
				float3 ambientLight = float3(0.2, 0.35, 0.4);
				float3 diffuseLight = ambientLight + diffuse;
				
				// direct spcular light // 眼睛看到的高光 // _WorldSpaceCameraPos // 主要是要知道現在這物體是哪些地方(frament)被render
				float3 camPos = _WorldSpaceCameraPos;
				float3 fragToCam = camPos - i.worldPos;
				float3 viewDir = normalize(fragToCam);
				

				// Phong
				float3 viewReflection = reflect(-viewDir, normalI); // 這個要做光暈還不錯 只要抓point away的視線
				float specularFalloff = max( 0, dot(viewReflection, sunDir) );
				specularFalloff = step(0.1, specularFalloff);//toony
				float gloss = pow(specularFalloff, _Gloss);
				
				
				//return float4(gloss.xxx, 0);



				// Blinn-Phong


				// composite
				
				float3 finalLight = diffuseLight * _Color.rgb + gloss;

				return float4(finalLight, 0); //記得 normal在反向的時候會是負數 所以加ambient light的時候 記得處理掉負數(用saturate()限制dot product，或是max(0, "wantedvalue")) 不然還會是黑的
            }
            ENDCG
        }
    }
}
