﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/UltimateScale"
{
	Properties
	{
		_Texture("Texture幹", 2D) = "White" {}
		_Color("Color", Color) = (1, 1, 1, 1)
		_POSITION("Position", Range(0, 1)) = 0.0
		_RANGE("Range", Range(0, 1)) = 0.5
		_noteLightIntensity("noteLightIntensity", Float) = 3
		_noteLightColor("noteLightColor", Color) = (1, 1, 1, 1)
		_noteLightColor2("noteLightColor2", Color) = (1 , 0, 1, 1)
		_ObjectScaleX("ObjectScaleX", Float) = 0
		_LightStartPoint("LightStartPoint", Float) = 0
		_Dir("LightGoDirection", Float) = 1
		//_SwapColorPos[1]("SwapColor", Int) = 0
		_NonNoteColor("NonNoteColor", Color) = (1, 1, 1, 1)
	}

	SubShader
	{
		Tags{" Queue" = " Geometry " "RenderType" = "Opaque"}
		LOD 100

		Pass
		{
			CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 because it uses wrong array syntax (type[size] name)
//#pragma exclude_renderers d3d11 // 這行到底再加三小幹你娘unity
// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
			//#pragma exclude_renderers d3d11 gles
			#pragma	vertex vert
			#pragma fragment frag

			// 記得propertie定義完後在這邊要宣告
			sampler2D _Texture; 
			float4 _Color;
			float _POSITION;
			float _RANGE;
			float _ObjectScaleX;
			float _LightStartPoint;
			uniform float _nowotePos[8]; // 用script傳值才有效
			float _nowotePosCount;
			float _SwapColorCount;
			float _noteLightIntensity;
			float4 _noteLightColor;
			float4 _noteLightColor2;
			uniform float _SwapColorPos[8];
			float _Dir;
			float4 _NonNoteColor;
			struct appdata { /* 其他軟體傳給你的資料。可以告訴他你想拿甚麼資料*/

				float4 vertex : POSITION;
				float2 uv : TEXCOORD0; /* TEXCOORD0 是第一個uv 也就是可能可以用範圍外的uv做一些事情*/
			};

			struct vert2fragment {

				float4 vertex : SV_POSITION;
				//float4 localPosition : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};


			
			vert2fragment vert(appdata i) /* 這邊才開始是實際function vert代表vert shader， frag 代表fragment shader 不可更改名稱*/
			{			
				vert2fragment v2f;
				v2f.uv = i.uv;
				//float4 objectOrigin = i.vertex - mul(unity_ObjectToWorld, float4(0.0, 0.0, 0.0, 1.0)); // 用vertex世界座標減掉pivot取得local座標 後面參數用fixe4存1.0會變成世界座標 WTF
				float4 objectOrigin = i.vertex - mul(unity_ObjectToWorld, float3(0.0, 0.0, 0.0));// 用vertex世界座標減掉pivot取得local座標
				
				_POSITION *= -_Dir; // 控制0~1的方向
				_RANGE *= -_ObjectScaleX;
				
				bool isNowote = false;
				
				for (int a = 0; a < /*_nowotePosCount*/1; a++) { // 要把空陣列減掉 不然會多一堆在原點 (所以現在另外傳一個nowotePosCount當長度)
					
					float offseted_POSITION = (_ObjectScaleX * /*_nowotePos[a]*/_POSITION) - (_LightStartPoint); // _nowotePos[a]這個陣列由譜bar而來
					
					if (_Dir < 0) {



						if (objectOrigin.x <= offseted_POSITION && objectOrigin.x >= offseted_POSITION - _RANGE) // 計算每一幀有哪幾顆vertex要亮起來 沒回圈的話只會有第一顆亮 所以每顆線上的譜都會讓這邊多迴圈一次
						{
							v2f.color = _Color * _noteLightColor * _noteLightIntensity;
							isNowote = true;
						}
						else if (isNowote == false)
						{
							v2f.color = _NonNoteColor;
						}
					}
					else {
						if (objectOrigin.x >= offseted_POSITION && objectOrigin.x <= offseted_POSITION + _RANGE) // 計算每一幀有哪幾顆vertex要亮起來 沒回圈的話只會有第一顆亮 所以每顆線上的譜都會讓這邊多迴圈一次
						{
							v2f.color = _Color * _noteLightColor * _noteLightIntensity;
							isNowote = true;
						}
						else if (isNowote == false)
						{
							v2f.color = _NonNoteColor;
						}
					}
				}
				v2f.vertex = UnityObjectToClipPos(i.vertex); //UnityObjectToClipPos(i.vertex);

				return v2f;
			}
			
			float4 frag(vert2fragment v) : SV_Target
			{
				
				return v.color; // 會變成範圍是選定的顏色 範圍外是另一個選定的顏色  幹話
			}

			ENDCG // 要做不只x軸的話 可能可以用的方式
		}


	}
}