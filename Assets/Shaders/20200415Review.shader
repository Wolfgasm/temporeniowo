﻿Shader "Unlit/20200415Review"
{
    Properties
    {
        _BaseColor("_BaseColor", COLOR) = ( 1, 1, 1, 1)
		_AmbientColor("AMBIENT", COLOR) = (0.2,0.2,0.2,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "UnityLightingCommon.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct VertexOutput
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 normal : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			VertexOutput vert (appdata v)
            {
				VertexOutput o;
				o.uv = v.uv; 
				//o.vertex = v.vertex; 錯誤
				o.vertex = UnityObjectToClipPos(v.vertex); // 這樣轉空間才對
				o.normal = v.normal;
                return o;
            }

			float4 _BaseColor;
			float4 _AmbientColor;
            float4 frag (VertexOutput i) : SV_Target
            {
				// sun
				float3 sundir = normalize(_WorldSpaceLightPos0); // 最好normorlize
				float3 sunFalloff = saturate(dot(sundir, i.normal));
				float3 diffuseLight = _LightColor0 * sunFalloff;
				
				// ambient
				float3 ambient = _AmbientColor; //+ diffuseLight;
				


				//float3 output = (diffuseLight + ambient) * _BaseColor.rgb; // 這次錯誤是diffuseLight+了兩次 不對 會看起來比較亮
				float3 output = (diffuseLight + ambient) * _BaseColor;
                return float4(output,0);
            }
            ENDCG
        }
    }
}
