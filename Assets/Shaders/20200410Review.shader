﻿Shader "Unlit/20200410Review"
{
    Properties
    {
		_DiffuseColor("DiffuseColor", COLOR) = (1,1,1,1)
		
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"
            struct VertexInput
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct VertexOutput
            {
                float2 uv : TEXCOORD0;
				float3 normal : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            
            VertexOutput vert (VertexInput v)
            {
				
                VertexOutput o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.normal = v.normal;
                
                return o;
            }

			float3 _DiffuseColor;

            float4 frag (VertexOutput i) : SV_Target
            {
               
				float3 normal = i.normal;
				//normal = (normal + 1) *0.5;

				float3 lightDir = _WorldSpaceLightPos0.xyz; // 我跟他習慣差在會不會打出xyz or rgb
				float3 lightColor = _LightColor0.rgb;
				float3 lightFalloff = saturate( dot(lightDir, normal)); // 雖然直接print falloff看不出來 但實際上黑色部分是太多的複數 到時候就算+ambient light還是會全黑
				float3 Light = lightColor *  lightFalloff;
				
				float3 ambientLight = float3(0.2, 0.35, 0.4);

				float3 Output = _DiffuseColor.rgb * (Light + ambientLight);

                return float4(Output,1);
            }
            ENDCG
        }
    }
}
