﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    public GameObject CamCollar;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = Vector3.Lerp(transform.position, CamCollar.transform.position, Time.deltaTime * 500.0f);
        transform.position = CamCollar.transform.position;
        transform.rotation = CamCollar.transform.rotation;
    }
}
