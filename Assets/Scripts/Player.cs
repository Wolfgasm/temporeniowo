﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class Player : MonoBehaviour
{
    public PathCreator Guard_laser_path;
    public float Laser_Gurad_Curve_num = 0.5f; // 0~1 // L to R

    public GameObject LaserGuard;
    public float laserGuardSpeed;

    public delegate void LaserGuardActive();

    public LaserGuardActive laserGuardActive;

    public GameObject tutorialImage;

    private float tutorialImage_timer = 6.0f;

    public bool playerLaserOn = false;
    public LineRenderer[] my_Lasers;
    public AnimationCurve laserSizeCurve;
    float laserSizeTimer = 0;

    void Start()
    {
        laserGuardActive += LaserGuardMove;
    }


    void Update()
    {
        if(tutorialImage_timer > 0)
        {
            tutorialImage_timer -= Time.deltaTime;
        }
        else
        {
            tutorialImage_timer = 0;
            tutorialImage.SetActive(false);
        }



        laserGuardActive?.Invoke();
        LaserGuardMove();

        // 真的不喜歡這樣寫 改天問一下Reddit
        if (playerLaserOn)
        {
            foreach (LineRenderer laser in my_Lasers)
            {
                laser.gameObject.SetActive(true);
                Vector3 mylaserTarget = this.transform.InverseTransformPoint(GameObject.FindGameObjectWithTag("Enemy").transform.position);
                laser.SetPosition(1, new Vector3(mylaserTarget.x-5.5f, mylaserTarget.y-15.0f, mylaserTarget.z + 150.0f));

                laserSizeTimer += Time.deltaTime / 3.0f;

                laser.endWidth = laserSizeCurve.Evaluate(laserSizeTimer) * 2.0f;

                laser.endWidth = Mathf.Clamp(laser.endWidth, 0, 10.0f); // 避免加到太大 ㄟ好像應該要讓他有曲線地加
                laser.startWidth = laser.endWidth;
            }
        }
    }

    public void LaserGuardInput()
    {
        // 控制雷射護衛 之後一定要寫黏上去的部分 不然速度一樣就永遠追不到 太快又要轉一下停一下
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Laser_Gurad_Curve_num -= Time.deltaTime * laserGuardSpeed;
            tutorialImage.SetActive(false);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            Laser_Gurad_Curve_num += Time.deltaTime * laserGuardSpeed;
            tutorialImage.SetActive(false);
        }

        // clamp在0.99就好 1會跑回原點
        Laser_Gurad_Curve_num = Mathf.Clamp(Laser_Gurad_Curve_num, 0, 0.99f);

    }

    public void LaserGuardMove()
    {
        // 將0~1換算成實際長度的%數
        float laser_Real_Distance = Guard_laser_path.path.length * Laser_Gurad_Curve_num;

        Vector3 laserGuardPos = Guard_laser_path.path.GetPointAtDistance(laser_Real_Distance);

        LaserGuard.transform.position = laserGuardPos;
    }

    public void LaserUltimate()
    {
        playerLaserOn = true;
        
    }
}
