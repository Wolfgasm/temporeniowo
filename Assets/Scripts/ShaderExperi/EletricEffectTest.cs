﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 
//[ExecuteAlways] // 用來把material改成instance的
public class EletricEffectTest : MonoBehaviour
{
    
    float[] noteShowBuffer = new float[8] { 2.0f , 2.0f, 2.0f, 2.0f, 2.0f, 2.0f, 2.0f, 2.0f}; // 譜的終點是 1.0 
    float[] swapColorPos = new float[8] { 1, 1, 1, 1, 1, 1, 1, 1 };
    int swapSwitch;
    float noteFlySpeed = 1 /0.8f; // 這個跟metronome的arrivetime相對
    public int barNum; // 應該要做一個 可以知道場上有幾個spawner的邏輯 不然想改都要去forgamemusic改一堆

   

    Metronome metronome;

    Renderer the_renderer; // 避免報警告所以+了個the_
    void Awake()
    {
       
        metronome = GameObject.FindGameObjectWithTag("GameController").GetComponent<Metronome>();
        the_renderer = GetComponent<Renderer>();
        metronome.SpawnNote += this.OnSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T)) // fixedupdate吃按鍵很遲鈍
        {
            bool sendbuffered = false;
            for(int i = 0 ; i < noteShowBuffer.Length; i++)
            {
                if(noteShowBuffer[i] >= 1 && sendbuffered == false)
                {
                    noteShowBuffer[i] = 0.0f;
                    sendbuffered = true;
                }
                
            }
            
        }


        for (int i = 0; i < noteShowBuffer.Length; i++)
        {
            noteShowBuffer[i] += Time.deltaTime * noteFlySpeed; // 飛行速度 跟歌配合用這邊應該就可以 主要是要想出怎麼旗標生成時機
        }

        the_renderer.material.SetFloatArray("_nowotePos", noteShowBuffer); // 只能用array不能list 不能改b的長度(第一次assign就決定了長度) 所以可能要用個buffer不能無腦加
        the_renderer.material.SetFloat("_nowotePosCount", noteShowBuffer.Length); // shader一定要宣告陣列長度 會在for迴圈出問題 所以陣列長度另外傳過去給for

        the_renderer.material.SetFloatArray("_swapColorPos", swapColorPos);
        //the_renderer.material.SetFloat("_swapColorPosCount", swapColorPos.Length); // shader一定要宣告陣列長度 會在for迴圈出問題 所以陣列長度另外傳過去給for


    }

    float PingPongOffset(float aValue, float aMin, float aMax) // 抄來的 只是給pingpong功能加一個offset
    {
        return Mathf.PingPong(aValue, aMax - aMin) + aMin;
    }

    
    public void OnSpawn(object sender, NotesEventArgs e)
    {
        if (e.spawners[barNum] == true) // 如果要改e.spawner 請去ForGameMusic
        {
            bool sendbuffered = false;
            for (int i = 0; i < noteShowBuffer.Length; i++)
            {
                if (noteShowBuffer[i] >= 1 && sendbuffered == false)
                {
                    noteShowBuffer[i] = 0.0f;
                    swapColorPos[i] = swapSwitch;

                    if (swapSwitch == 0)
                    {
                        swapSwitch = 1;
                        //Debug.Log("set_0");
                    }
                    else if(swapSwitch == 1)
                    {

                        swapSwitch = 0;
                        //Debug.Log("set_1");
                    }

                    sendbuffered = true;
                }

            }
        }
    }
}
