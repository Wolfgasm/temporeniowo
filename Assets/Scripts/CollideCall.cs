﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideCall : MonoBehaviour
{
    public event System.EventHandler<CollideCall> collisionEnterEvent;
    public event System.EventHandler<CollideCall> collisionExitEvent;
    public event System.EventHandler<CollideCall> collisionStayEvent;
    public string _CollideEventName;

    StageManager stageManager; // 重要 繼承寫法就可以讓這個script通用各個Scene 
    //因為我要從分散的腳本拿每個scene的controller 但可惜他們現在都是不同類別
    void Start()
    {
        stageManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<StageManager>();
        collisionEnterEvent += stageManager.PopGUI;
        collisionExitEvent += stageManager.CloseGUI;
    }


    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        collisionEnterEvent?.Invoke(this, this);
    }
    private void OnTriggerExit(Collider other)
    {
        collisionExitEvent?.Invoke(this, this);
    }

    private void OnTriggerStay(Collider other)
    {
        collisionStayEvent?.Invoke(this, this);
    }

    
}
