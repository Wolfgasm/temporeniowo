﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hovering : MonoBehaviour
{
    public float yAmount;
    public float pingpong;
    float yoffset;
    void Start()
    {
        yoffset = transform.position.y - (yAmount / 2);
    }

    // Update is called once per frame
    void Update()
    {
        pingpong = Mathf.PingPong(Time.time, yAmount);
        //Debug.Log("WTF");
        this.transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, yoffset + pingpong, Time.deltaTime / 6.0f), transform.position.z);
    }
}
