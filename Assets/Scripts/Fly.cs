﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;


[ExecuteInEditMode]
public class Fly : MonoBehaviour
{
    public PathCreator pathCreator;
    public float setflySpeed, throttle;
    [SerializeField]
    float nowFlySpeed = 0;
    [SerializeField]
    public float plane_Distance_Travelled;
    public float enemy_Distance_Travelled = 14; // 有值是因為要讓他比玩家前面
    //public GameObject Gameobject_TO_Controll;
    public GameObject Plane;
    public GameObject enemy;
    public GameObject Speed_Reacting_camera, camOrigin;
    public float overAllRotationSpeed = 150.0f;

    // 通道顯示
    public GameObject tunnel;

    // 攝影機偏移
    public float Cam_Backward_Length_Max = 0.5f;
    public float Cam_Rotate_X_Max = 5.0f;
    public float Cam_rotate_Z_MAX = 10.0f;
    float lastZ;

    [Header("速度設定")]
    public Vector2[] speedArea; // x是距離y是速度
    private int speedAreaFlag;

    private void Awake()
    {
        if (Application.isPlaying)
        {
            speedAreaFlag = 0;
            Debug.Log(pathCreator.path.length + "  線總長");
        }
        
    }
    void Update() // 務必要Fixed不然雷射會跟不上
    {

#if (UNITY_EDITOR)
        for(int i = 0; i < speedArea.Length; i++)
        {
            for(int a = i; a < speedArea.Length; a++)
            {
                if(speedArea[i].x > speedArea[a].x)
                {
                    Vector2 temp = speedArea[a];
                    speedArea[a] = speedArea[i];
                    speedArea[i] = temp;
                }
            }
        }
        //Debug.Log("aaa");
#endif
        if (Application.isPlaying)
        {
            nowFlySpeed = Mathf.Lerp(nowFlySpeed, setflySpeed, Time.deltaTime); //* 5 = 0.2s * 2 = 0.5s // 之後看這裡能不能改良一下 
            plane_Distance_Travelled += nowFlySpeed * Time.deltaTime;
            enemy_Distance_Travelled += nowFlySpeed * Time.deltaTime;

            Plane.transform.position = pathCreator.path.GetPointAtDistance(plane_Distance_Travelled);
            enemy.transform.position = pathCreator.path.GetPointAtDistance(enemy_Distance_Travelled);

            // 可以設定每一段的速度
            if (speedAreaFlag < speedArea.Length)
            {
                if (plane_Distance_Travelled >= speedArea[speedAreaFlag].x)
                {
                    setflySpeed = speedArea[speedAreaFlag].y;
                    speedAreaFlag++;

                    
                }
            }
            if (plane_Distance_Travelled >= 5000 && plane_Distance_Travelled <= 5010.0f)
            {
                tunnel.SetActive(true);
            }
            if (plane_Distance_Travelled >= 7300 && plane_Distance_Travelled <= 7310.0f)
            {
                tunnel.SetActive(false);
            }


            //Gameobject_TO_Controll.transform.rotation = Quaternion.Lerp(Gameobject_TO_Controll.transform.rotation, PathCreator.path.GetRotationAtDistance(Distance_Travelled), Time.time / overAllRotationSpeed); 
            // 之後要把敵人跟自己分開 不要在同一個物件了

            float camBackward = (setflySpeed - nowFlySpeed) / 100.0f * Cam_Backward_Length_Max;
            if (camBackward <= 0)
                camBackward /= 3.5f;
            float cam_Rotate_x = (setflySpeed - nowFlySpeed) / 100.0f * Cam_Rotate_X_Max;


            float cam_rotate_z = (Speed_Reacting_camera.transform.rotation.z - lastZ) / 100.0f * Cam_rotate_Z_MAX;
            if (cam_rotate_z >= Cam_rotate_Z_MAX || cam_rotate_z <= -Cam_rotate_Z_MAX)
                cam_rotate_z = Cam_rotate_Z_MAX;


            Vector3 transform_reactAmount = new Vector3(
                camOrigin.transform.localPosition.x,
                camOrigin.transform.localPosition.y + camBackward,
                camOrigin.transform.localPosition.z - camBackward);

            Vector3 rotate_reactAmount = new Vector3(
                camOrigin.transform.localEulerAngles.x + cam_Rotate_x,
                camOrigin.transform.localEulerAngles.y,
                camOrigin.transform.localEulerAngles.z);
            // 偏移
            Speed_Reacting_camera.transform.localPosition = Vector3.Lerp(Speed_Reacting_camera.transform.localPosition, transform_reactAmount, Time.deltaTime / 1f);
            // 偏轉
            Speed_Reacting_camera.transform.localEulerAngles = Vector3.Lerp(Speed_Reacting_camera.transform.localEulerAngles, rotate_reactAmount, Time.deltaTime / 1f);

            //float lerpz = Mathf.Lerp(Speed_Reacting_camera.transform.localEulerAngles.z, cam_rotate_z, Time.deltaTime);
            // Z軸偏轉(不跟上面一起是因為他會想翻一圈) // 我根本不知道怎成功的btw
            Speed_Reacting_camera.transform.localEulerAngles = new Vector3(
                Speed_Reacting_camera.transform.localEulerAngles.x,
                Speed_Reacting_camera.transform.localEulerAngles.y,
                cam_rotate_z * 200.0f);

            lastZ = camOrigin.transform.localEulerAngles.z;
            // 注意每幀要很慢的lerp到目標角度 不然會一卡一卡的 
            //Plane.transform.rotation = Quaternion.Lerp(Plane.transform.rotation, PathCreator.path.GetRotationAtDistance(Distance_Travelled), Time.time / 30.0f);

            Plane.transform.rotation = Quaternion.Lerp(Plane.transform.rotation, pathCreator.path.GetRotationAtDistance(plane_Distance_Travelled), Time.time / 30.0f);
            enemy.transform.rotation = Quaternion.Lerp(Plane.transform.rotation, pathCreator.path.GetRotationAtDistance(enemy_Distance_Travelled), Time.time / 45.0f);
        }
    }
    
}
