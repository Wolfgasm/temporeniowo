﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ResultManage : MonoBehaviour
{
    /*public int perfectNum;
    public int goodNum;
    public int missNum;*/

    private int[] nums = new int[3];
    public Text perfectText;
    public Text goodText;
    public Text missText;
    public Slider ultimateSlider;
    public GameObject bossImage;
    public GameObject bossRotateDummyTarget;
    public GameObject StuffGotoDummyTarget;
    //public GameObject resultGotoDummyTarget;
    public Image winLoseImage;
    public Sprite loseSprite;
    NumParseGuy parseGuy;

    public AnimationCurve addingSpeedCurve;

    public GameObject firstPage;
    public bool doneCounting = false;
    public bool startUltimateCount = false;
    public bool showBoss = false;
    public bool showWinLose = false;
    public float firstPageExitposX = -2000;

    Image fadingImage;
    public NumParseGuy numParseGuy;
    void Start()
    {
        
        parseGuy = GameObject.FindGameObjectWithTag("parseGuy").GetComponent<NumParseGuy>();
        
        StartCoroutine(ICountTimer());

        try
        {
            fadingImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
            // Fade效果
            fadingImage.gameObject.GetComponent<Animator>().SetTrigger("FadeIn");
        }
        catch {
            Debug.Log("沒有fadingimage");
        }
        StartCoroutine(SetFade());
    }


    void Update()
    {


        //float eachPerfectNoteTimePos = 1.0f / parseGuy.perfectNum;
        ///nowPerfectNoteTimePos = eachPerfectNoteTimePos * (nums[0]);

        

        perfectText.text = nums[0].ToString();
        goodText.text = nums[1].ToString();
        missText.text = nums[2].ToString();
        if (doneCounting)
        {
            Vector3 temp = new Vector3(Mathf.Lerp(firstPage.transform.position.x, firstPageExitposX, Time.deltaTime ), firstPage.transform.position.y, firstPage.transform.position.z);
            firstPage.transform.position = temp;

            Vector3 tempSlider = new Vector3(Mathf.Lerp(ultimateSlider.transform.position.x, StuffGotoDummyTarget.transform.position.x, Time.deltaTime * 3.0f), ultimateSlider.transform.position.y, ultimateSlider.transform.position.z);
            ultimateSlider.transform.position = tempSlider;
        }

        if (startUltimateCount)
        {
            float temp = Mathf.Lerp(ultimateSlider.value, parseGuy.UltimateNum, Time.deltaTime);
            ultimateSlider.value = temp;
        }

        if (showBoss)
        {
            Vector3 temp = Vector3.Lerp(bossImage.transform.position, StuffGotoDummyTarget.transform.position, Time.deltaTime * 3.0f);
            Quaternion rotateTemp = Quaternion.Lerp(bossImage.transform.rotation, bossRotateDummyTarget.transform.rotation , Time.deltaTime * 3.0f);

            bossImage.transform.position = temp;
            bossImage.transform.rotation = rotateTemp;
        }

        if (showWinLose)
        {
            if (parseGuy.winOrLose == false)
            {
                winLoseImage.sprite = loseSprite;
                //Debug.Log("loseSprite");
            }
            showWinLose = true;
            float alpha = Mathf.Lerp(winLoseImage.color.a, 1,Time.deltaTime * 8.0f);
            winLoseImage.color = new Vector4(1, 1, 1, alpha);
        }
    }

    IEnumerator ICountTimer()
    {
        yield return new WaitForSeconds(3.0f);
        // perfect
        while (nums[0] < parseGuy.perfectNum)
        {
            
            float nowTimePos = (1.0f / parseGuy.perfectNum) * (nums[0]);

            nums[0] += 1;
            yield return new WaitForSeconds(1 - addingSpeedCurve.Evaluate(nowTimePos));
        }
        Debug.Log("add perfect done");
        yield return new WaitForSeconds(1.0f);

        // good
        while (nums[1] < parseGuy.goodNum)
        {
            float nowTimePos = (1.0f / parseGuy.goodNum) * (nums[1]);

            nums[1] += 1;
            yield return new WaitForSeconds(1 - addingSpeedCurve.Evaluate(nowTimePos));
        }
        Debug.Log("add good done");
        yield return new WaitForSeconds(1.0f);

        // miss
        while (nums[2] < parseGuy.missNum)
        {
            float nowTimePos = (1.0f / parseGuy.missNum) * (nums[2]);

            nums[2] += 1;
            yield return new WaitForSeconds(1 - addingSpeedCurve.Evaluate(nowTimePos));
        }
        Debug.Log("add miss done");

        yield return new WaitForSeconds(3.0f);
        doneCounting = true;

        Debug.Log("dont all Counting");

        yield return new WaitForSeconds(3.0f);
        startUltimateCount = true;
        yield return new WaitForSeconds(5.0f);
        showBoss = true;
        yield return new WaitForSeconds(3.0f);
        showWinLose = true;
        yield return new WaitForSeconds(3.0f);
        try
        {
            fadingImage.gameObject.GetComponent<Animator>().SetTrigger("FadeOut");
        }
        catch
        {
            Debug.Log("沒有fade圖");
        }
        yield return new WaitForSeconds(1.5f);

        SceneManager.LoadScene("Stage");

        //Destroy(fadingImage.gameObject);

        StopAllCoroutines();
    }

    IEnumerator SetFade()
    {
        yield return new WaitForSeconds(0.2f);
        try
        {
            fadingImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
            // Fade效果
            fadingImage.gameObject.GetComponent<Animator>().SetTrigger("FadeIn");

        }
        catch
        {
            Debug.Log("沒有fadingimage");
        }
    }

    
    
}
