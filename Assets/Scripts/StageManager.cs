﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyCharacter;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering;
public class StageManager : MonoBehaviour
{
    

    public GameObject pressF_Image;
    public GameObject chooseSong_Image;
    public NaughtyCharacter.Character prota;
    bool canTouchWeapon = false;
    bool isChoosingSong = false;

    public Image fadingImage;
    public GameObject circle;

    // 換場景的天空
    public GotoMain gotomain;
    public CubemapParameter easy_cubeMap,hard_cubeMap;
    void Start()
    {

        // 避免舊流程的跨場景物件影響新遊戲 我測好久烏烏烏烏
        GameObject[] fades = GameObject.FindGameObjectsWithTag("Fade");
        if (fades.Length > 1)
        {
            for(int i = 0; i < fades.Length; i++)
            {
                if(fades[i].GetInstanceID() == fadingImage.gameObject.GetInstanceID())
                {
                    Destroy(fades[i].gameObject);
                }
                
            }
            
        }

        GameObject[] gotoMain = GameObject.FindGameObjectsWithTag("scriptableSavior");
        if (gotoMain.Length > 1)
        {
            for (int i = 0; i < gotoMain.Length; i++)
            {
                if(gotoMain[i].GetInstanceID() == gotomain.gameObject.GetInstanceID())
                {
                    Destroy(gotoMain[i].gameObject);
                }
                
            }
                
        }
        GameObject[] parseGuy = GameObject.FindGameObjectsWithTag("parseGuy");
        if (parseGuy!= null)
        {
            for (int i = 0; i < parseGuy.Length; i++)
            {
                Destroy(parseGuy[i].gameObject);
            }
            
        }


        fadingImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
        gotomain = GameObject.FindGameObjectWithTag("scriptableSavior").GetComponent<GotoMain>();
        if (fadingImage.color.a >= 0.5f)
        {
            fadingImage.gameObject.GetComponent<Animator>().SetTrigger("FadeIn");
        }
        
        pressF_Image.SetActive(false);
        chooseSong_Image.SetActive(false);
        
    }


    void Update()
    {
        if (isChoosingSong && Input.GetKeyDown(KeyCode.F))
        {

            UnChooseDifficulty();
        }

        else if (canTouchWeapon && Input.GetKeyDown(KeyCode.F))
        {
            ChooseDifficulty();  
        }

        if (isChoosingSong)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Debug.Log("選了簡單");
                GameObject clone = Instantiate(circle, transform);
                clone.transform.position = prota.transform.position;
                isChoosingSong = false;
                StartCoroutine(GotoNextScene(4.0f));
                pressF_Image.SetActive(false);
                chooseSong_Image.SetActive(false);
                gotomain.cubeMap = easy_cubeMap;
                gotomain.jsonFileName = "testOutput_ReallyEasy.json";
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("選了困難");
                GameObject clone = Instantiate(circle, transform);
                clone.transform.position = prota.transform.position;
                isChoosingSong = false;
                StartCoroutine(GotoNextScene(4.0f));
                pressF_Image.SetActive(false);
                chooseSong_Image.SetActive(false);
                gotomain.cubeMap = hard_cubeMap;
                gotomain.jsonFileName = "testOutput_hard.json";
            }
        }
        

    }

    public void PopGUI(object sender, CollideCall e)
    {
        if(e._CollideEventName == "pressF")
        {
            pressF_Image.SetActive(true);
            canTouchWeapon = true;
            Debug.Log("Call Press F");
        }
    }

    public void CloseGUI(object sender, CollideCall e)
    {
        if (e._CollideEventName == "pressF")
        {
            pressF_Image.SetActive(false);
            canTouchWeapon = false;
            Debug.Log("Call close F");
        }

    }

    public void ChooseDifficulty()
    {
        //canTouchWeapon = false;
        pressF_Image.SetActive(true);
        chooseSong_Image.SetActive(true);
        prota.MovementSettings.MaxHorizontalSpeed = 0;
        isChoosingSong = true;
    }

    public void UnChooseDifficulty()
    {
        pressF_Image.SetActive(true);
        chooseSong_Image.SetActive(false);
        prota.MovementSettings.MaxHorizontalSpeed = 8;
        isChoosingSong = false;
    }

    IEnumerator GotoNextScene(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        
        fadingImage.gameObject.GetComponent<Animator>().SetTrigger("FadeOut");
        yield return new WaitForSeconds(1.2f);
        SceneManager.LoadScene("Main");

        yield return new WaitForSeconds(1.2f);

        
    }
}
