﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class FrontPage : MonoBehaviour
{
    public Text pressSpace;
    float pinpong = 0;
    public GameObject cam;
    public Transform camCenter;
    public float rotateSpeed;

    AudioSource audioSource;
    public AudioClip spaceSoundClip;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.time = 60.0f;
    }

 
    void Update()
    {
        pinpong =  Mathf.PingPong(Time.time, 1.0f) /** Time.deltaTime*/;
        pressSpace.color = new Vector4(1, 1, 1, pinpong);
        //Debug.Log(pinpong);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(ChangeSceneTimer());

            audioSource.clip = spaceSoundClip;
            audioSource.time = 0;
            audioSource.Play();
        }


        // 音量調整保險
        if (Input.GetKey(KeyCode.F1) && Input.GetKey(KeyCode.F2) && Input.GetKeyDown(KeyCode.UpArrow))
        {
            audioSource.volume += 0.01f;
        }
        else if (Input.GetKey(KeyCode.F1) && Input.GetKey(KeyCode.F2) && Input.GetKeyDown(KeyCode.DownArrow))
        {
            audioSource.volume -= 0.01f;
        }

        cam.transform.RotateAround(camCenter.position, transform.up, Time.deltaTime * rotateSpeed);
    }

    IEnumerator ChangeSceneTimer()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("Stage");
        //Collider a;
        
    }
}
