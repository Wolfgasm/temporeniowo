﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DJPADS : MonoBehaviour
{
    // Start is called before the first frame update
    public Renderer[] padsRenderer;

    Color[] c;
    void Start()
    {
        c = new Color[padsRenderer.Length];
        RegeneratePadsColor();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < padsRenderer.Length; i++)
        {
            Color tempcolor = Vector4.Lerp(padsRenderer[i].material.GetColor("_EmissiveColor"), c[i], Time.deltaTime * 5.0f);
            padsRenderer[i].material.SetColor("_EmissiveColor", tempcolor);
        }
    }

    public void RegeneratePadsColor()
    {
        for(int i = 0; i < padsRenderer.Length; i++)
        {
            c[i] = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            c[i] *= 1.2f;
            //padsRenderer[i].material.SetColor("_EmissiveColor", c[i] * 1.2f);
        }
        
    }
}
