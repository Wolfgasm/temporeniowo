﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : MonoBehaviour
{
    public Transform target;

    public float speed = 5f;
    public float rotateSpeed = 200f;

    private Rigidbody rb;

    public string targetTag_name;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        target = GameObject.FindGameObjectWithTag(targetTag_name).transform;
    }

    void FixedUpdate()
    {
        Vector3 direction = target.position - rb.position;

        /*if (this.targetTag_name == "EnergyCollectR" || this.targetTag_name == "EnergyCollectL")
        {
            direction = target.localPosition - rb.gameObject.transform.localPosition;

        }*/
        //Vector3 direction = target.position - rb.position;

        direction.Normalize();

        Vector3 rotateAmount = Vector3.Cross(direction, transform.up);

        rb.angularVelocity = -rotateAmount * rotateSpeed;

        rb.velocity = transform.up * speed;
    }

    void OnTriggerEnter(Collider co)
    {
        if(this.targetTag_name == "EnergyCollectR")
        {
            if(co.gameObject.tag == "EnergyCollectR")
            {
                Destroy(gameObject);
            }
        }
        else if (this.targetTag_name == "EnergyCollectL")
        {
            if (co.gameObject.tag == "EnergyCollectL")
            {
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
        
    }
}
