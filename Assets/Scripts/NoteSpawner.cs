﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public  class NoteSpawner : MonoBehaviour
{
    public int thisSpawnerNumber;
    public GameObject genericNote;
    public GameObject receiver;

    [SerializeField]
    Metronome _metronome = null;
    void Awake()
    {
        _metronome = GameObject.FindGameObjectWithTag("GameController").GetComponent<Metronome>();
        this._metronome.SpawnNote += this.OnSpawn;
    }


    void Update()
    {

    }

    public void OnSpawn(object sender, NotesEventArgs e)
    {
        if (thisSpawnerNumber != -1)
        {
            if (e.spawners[thisSpawnerNumber] == true) // 生成的時候要在combatzone裡面//--------------------------------------------重要////////////////////
            {
                GameObject nowNote = Instantiate(genericNote, transform.position, transform.rotation);
                nowNote.GetComponent<NoteInstance>().noteBoopTargetPos = receiver.transform;

                nowNote.transform.parent = GameObject.FindGameObjectWithTag("CombatZone").transform;
                //e.theNoteOnthisSquare.spawnner = thisSpawnerNumber; // 生成譜時順便紀錄生成者;
            }
        }

    }

 


}
