﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class Enemy : MonoBehaviour
{
    public PathCreator player_laser_path;
    public PathCreator player_Guard_path;
    public float Laser_At_Target_Curve; // 0~1 // L to R
    public bool laserOn = false;
    public bool canDamage = false;
    public float laserOnPeriod;
    float laserSizeTimer = 0; // 用在animation curve的 realtime0~1秒 用curve輸出數值
    float laserPreWarnTime = 0; // 雷射還沒開始有傷害的提示時間
    public float laserMaxWidth;
    public int swipeDir = 1;
    public float swipeSpeed = 1;
    public float laserPreSwipeTimer;

    public AnimationCurve laserSizeCurve;

    public LineRenderer attackLaser;
    public Player player;
    public Metronome gameController;

    // 雷射起點物件
    public Transform laserStartPoint;

    // 玩家身上的鐵欄杆閃爍
    public Renderer pollRenderer;
    Material pollOriginalMaterial;
    public Material pollFlashMaterial;
    bool flashRed;
    bool startFlashing;
    Coroutine co;

    // 護盾的animator
    public GameObject guardItself;
    Animator guardAnimator;

    void Start()
    {
        //Debug.Log(player_laser_path.path.length);
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<Metronome>();
        gameController.SpawnLaser += this.LaserEventRecevie;
        pollOriginalMaterial = pollRenderer.material;
        guardAnimator = guardItself.GetComponent<Animator>();
    }


    void Update()
    {
        // clamp在0.99就好 1會跑回原點
        Laser_At_Target_Curve = Mathf.Clamp(Laser_At_Target_Curve, 0, 0.99f);

        // 測試用 pingpong展示雷射掃動
        //Laser_At_Target_Curve = Mathf.PingPong(Time.time * 0.5f, 0.99f);

        // 讓他自動到邊界後就回彈
        if (Laser_At_Target_Curve >= 0.99f)
            swipeDir *= -1;
        else if (Laser_At_Target_Curve <= 0)
            swipeDir *= -1;



        // 將0~1換算成實際長度的%數
        float laser_Real_Distance = player_laser_path.path.length * Laser_At_Target_Curve;

        //Vector3 laserHitPos = player_laser_path.path.GetPointAtDistance(laser_Real_Distance);
        Vector3 laserHitPos;


        attackLaser.SetPosition(0, this.transform.InverseTransformPoint(laserStartPoint.position));

        if (Input.GetKeyDown(KeyCode.T))
        {
            StartCoroutine(AttackingPlayer(0.5f));
        }

        // 被呼叫雷射時觸發這裡
        if (laserOn)
        {
            // snap on
            if (player.Laser_Gurad_Curve_num <= Laser_At_Target_Curve + 0.05f && player.Laser_Gurad_Curve_num >= Laser_At_Target_Curve - 0.05f && swipeDir == -1 && Input.GetKey(KeyCode.LeftArrow))
            {
                player.Laser_Gurad_Curve_num = Laser_At_Target_Curve;
                player.laserGuardActive -= player.LaserGuardInput;
                if (co != null)
                {
                    StopCoroutine(co);
                }

                startFlashing = false;
                canDamage = false;
                laserHitPos = player_Guard_path.path.GetPointAtDistance(laser_Real_Distance);
                guardAnimator.SetBool("IsGuarding", true);
                pollRenderer.material = pollOriginalMaterial;
            }
            else if (player.Laser_Gurad_Curve_num <= Laser_At_Target_Curve + 0.05f && player.Laser_Gurad_Curve_num >= Laser_At_Target_Curve - 0.05f && swipeDir == 1 && Input.GetKey(KeyCode.RightArrow))
            {
                player.Laser_Gurad_Curve_num = Laser_At_Target_Curve;
                // 記得關掉原本的移動輸入
                player.laserGuardActive -= player.LaserGuardInput;
                if (co != null)
                {
                    StopCoroutine(co);
                }
                startFlashing = false;
                canDamage = false;

                laserHitPos = player_Guard_path.path.GetPointAtDistance(laser_Real_Distance);
                guardAnimator.SetBool("IsGuarding", true);
                pollRenderer.material = pollOriginalMaterial;
            }
            else
            {
                // 被打到

                player.laserGuardActive = player.LaserGuardInput;
                if(laserPreWarnTime <= 0)
                {
                    canDamage = true;
                }

                laserHitPos = player_laser_path.path.GetPointAtDistance(laser_Real_Distance);
                guardAnimator.SetBool("IsGuarding", false);
            }

            if(canDamage == true)
            {
                if(startFlashing == false)
                {
                    co = StartCoroutine(AttackingPlayer(0.05f));
                    startFlashing = true;
                }

                if(Metronome.nowUltimateNum > 0.1f)
                {
                    Metronome.nowUltimateNum -= 0.1f * Time.deltaTime;
                }
                
            }


            laserSizeTimer += Time.deltaTime;
            attackLaser.SetPosition(1, this.transform.InverseTransformPoint(laserHitPos)); // 將目標的世界座標轉為自己的local座標在放到雷射上 這樣就不用搞雷射的worldpos
            attackLaser.endWidth = laserSizeCurve.Evaluate(laserSizeTimer);
            attackLaser.endWidth = Mathf.Clamp(attackLaser.endWidth, 0, laserMaxWidth); // 避免加到太大 ㄟ好像應該要讓他有曲線地加

            // 時間到關閉雷射
            laserOnPeriod -= Time.deltaTime;
            laserPreWarnTime -= Time.deltaTime;
            if (laserOnPeriod <= 0 - laserPreSwipeTimer)
            {
                laserOnPeriod = 0 - laserPreSwipeTimer;
                laserOn = false;
                canDamage = false;
                if (co != null)
                {
                    StopCoroutine(co);
                }
                //StopAllCoroutines();
                guardAnimator.SetBool("IsGuarding", false);

                pollRenderer.material = pollOriginalMaterial;
            }

            // 新招 直接用animation curve幫我做雷射縮減動畫
            //Debug.Log(laserSizeCurve.Evaluate(0.7f));

            // prewarn時間到才開始掃動
            if (laserPreWarnTime <= 0)
            {
                canDamage = true;
                if (swipeDir == 1)
                    Laser_At_Target_Curve += Time.deltaTime * swipeSpeed; // 這邊之後要設置一下可控速度
                else
                    Laser_At_Target_Curve -= Time.deltaTime * swipeSpeed;
            }
        }
        else
        {
            attackLaser.SetPosition(1, new Vector3(0, 0, 0)); // 將目標的世界座標轉為自己的local座標在放到雷射上 這樣就不用搞雷射的worldpos
            attackLaser.endWidth = 0;
            laserSizeTimer = 0;

            player.laserGuardActive = player.LaserGuardInput;
            canDamage = false;
            startFlashing = false;


        }

        /*if (Input.GetKeyDown(KeyCode.L))
        {
            LaserStart(0.7f, -1, 0.7f, 160.0f, 96.0f);
        }*/
    }

    public void LaserEventRecevie(object sender, NotesEventArgs e)
    {
        LaserStart(e.startPoint, e.swipeDir, e.swipeSpeed, e.periodInMeasure32, e.warningTimeInMeasure32);
    }

    // 0 << >> 1 // 雷射開始時間被我改成用拍子 這要到時候noteEditor寫就放在正確的位置 正式遊戲在把appeartime依照warningTime往前放就好
    public void LaserStart(float _startPoint, int _swipeDir, float _swipeSpeed, float _periodInMeasure32, float _warningTimeInMeasure32)
    {

        laserPreWarnTime = _warningTimeInMeasure32 * gameController.nowplay.SongquarterNoteTime;

        laserOnPeriod = _periodInMeasure32 * gameController.nowplay.SongquarterNoteTime + laserPreWarnTime;

        laserOn = true;

        swipeDir = _swipeDir;

        swipeSpeed = _swipeSpeed;

        Laser_At_Target_Curve = _startPoint; // 可能定義起點跟時間以及方向 而不是起點 終點 速度 注意還有預警要處理
    }

    IEnumerator AttackingPlayer(float _flashInterval)
    {
        yield return new WaitForSeconds(_flashInterval);
        while (true)
        {
            if (flashRed == false)
            {
                pollRenderer.material = pollFlashMaterial;
                flashRed = true;
            }
            else
            {
                pollRenderer.material = pollOriginalMaterial;
                flashRed = false;
            }
            yield return new WaitForSeconds(_flashInterval);
        }
       
        

    }
}
