﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System; // the convert class

//[ExecuteInEditMode]
public class SerialCom : MonoBehaviour
{
    [SerializeField]
    private string[] coms;
    public string useport;
    public int baudrate;
    public int databits;
    public StopBits stopbits;
    public Parity parity;

    SerialPort port1;

    // debug
    float updateRate = 0;
    public ParticleSystem particle;
    public float hsvS = 0;

    public string a;
    bool toggle = false;
    void Start()
    {
        port1 = new SerialPort(useport, baudrate, parity, databits, stopbits);
        //port1.Open();
        try
        {
            /*coms = SerialPort.GetPortNames();
            port1.PortName = useport;
            port1.BaudRate = baudrate;
            port1.DataBits = databits;
            port1.StopBits = stopbits;
            port1.Parity = parity;*/
            port1.Open();
            Debug.Log("Port OPENED");
        }
        catch
        {
            Debug.Log("Port OPEN FAILED");
        }


        
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            a = port1.ReadExisting();
            
            if(a.Substring(0,1) == "1" && a.Substring(7, 1) == "0")
            {
                
               // Debug.Log(a + "更新秒數 " + updateRate);
                if(a.Substring(0, 8) == "10000000")
                {
                    if(hsvS <= 0.95f)
                    {
                        hsvS += 0.05f;
                    }

                    // 現在要改particle color要從他的main module改 煩死
                    ParticleSystem.MainModule particleMain = particle.main;
                    particleMain.startColor = Color.HSVToRGB(hsvS, 1.0f, 1.0f);

                    Debug.Log(hsvS + "++ ");
                }
                else if(a.Substring(0, 8) == "11000000")
                {
                    if (hsvS >= 0.05f)
                    {
                        hsvS -= 0.05f;
                    }

                    // 現在要改particle color要從他的main module改 煩死
                    ParticleSystem.MainModule particleMain = particle.main;
                    particleMain.startColor = Color.HSVToRGB(hsvS, 1.0f, 1.0f);

                    Debug.Log(hsvS + "-- ");
                }
            }
            
            updateRate = 0;
        }
        catch
        {
            updateRate += Time.deltaTime;
        }
       

    }

    public void ToggleFan()
    {
        if(toggle == false)
        {
            port1.WriteLine("D");
            toggle = true;
        }
        else
        {
            port1.WriteLine("d");
            toggle = false;
        }
       
    }
}
