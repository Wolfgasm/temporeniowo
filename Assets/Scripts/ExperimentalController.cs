﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentalController : MonoBehaviour
{
    public GameObject buildingsRef;
    public float building_Blocks_Distance;
    public float Start_buiding_Group_Num;
    public float Start_offset_Position;
    Random ran = new Random();

    private Transform Last_Block_Transform;
    
    void Start()
    {
        Create_New_Buildings(Start_buiding_Group_Num, building_Blocks_Distance, Start_offset_Position);
        
    }

   
    void FixedUpdate()
    {
        if(Last_Block_Transform.position.z <= 2500.0f) // 2500是目前生成新建築的點，之後可以動態算反正先固定
        {
            
        }
    }

    void Create_New_Buildings(float times, float zSpace, float _offsetPosition)
    {
        for (int i = 0; i < times; i++)
        {
            GameObject clonetemp = Instantiate(buildingsRef, new Vector3(0, 0, ((i+1) * zSpace) + _offsetPosition), transform.rotation);
            clonetemp.transform.eulerAngles = new Vector3(transform.rotation.x, Random.Range(0, 4) * 90, transform.rotation.z);
            Last_Block_Transform = clonetemp.transform;
        }
    }


}
