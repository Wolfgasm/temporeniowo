﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltimateIndicate : MonoBehaviour
{
    Renderer the_renderer;
    public float indicateNum = 0.8f;

    float spectrumLerped;

    // 跟metronome拿音樂特定頻段
    Metronome metronome;
    public int spectrumFromMetronome;
    public float theSpectrumFloat;
    float lastVolume;
    void Awake()
    {
        the_renderer = GetComponent<Renderer>();
        metronome = GameObject.FindGameObjectWithTag("GameController").GetComponent<Metronome>();
        //Debug.Log(metronome.spectrum.Length);
        lastVolume = metronome.LevelAudioSource.volume;
    }


    void Update()
    {
        indicateNum = Metronome.nowUltimateNum;

        //theSpectrumFloat = Mathf.Round(metronome.spectrum[spectrumFromMetronome] * 10000.0f) * 1f;
        //Debug.Log(metronome.spectrum.Length);
        //try
        //{
        theSpectrumFloat = (metronome.spectrum[spectrumFromMetronome] * (0.02f / metronome.LevelAudioSource.volume)) * (5000 * (0.02f / metronome.LevelAudioSource.volume));
        Debug.Log("WHY wrong >> " + (5000 * (0.02f / metronome.LevelAudioSource.volume)));
        //theSpectrumFloat *= 0.02f / metronome.LevelAudioSource.volume;
       // }
        //catch { Debug.Log("incorrect Load"); }

        //Debug.Log(theSpectrumFloat);
        // 把小數控制在6個0
        //try { 
        /*string temp = theSpectrumFloat.ToString("#.#######");
        theSpectrumFloat = float.Parse(temp) * 6500;*/
        //}
        //catch
        //{
           // Debug.Log("exception in UltimateIndicare, but its ok");
        //}

        if(metronome.LevelAudioSource.volume != lastVolume)
        {
            spectrumLerped = 0;
            lastVolume = metronome.LevelAudioSource.volume;
        }
        spectrumLerped = Mathf.Lerp(spectrumLerped, theSpectrumFloat, Time.deltaTime * 1.0f);
        spectrumLerped = Mathf.Lerp(spectrumLerped, 0, Time.deltaTime * 8.0f);
        //spectrumLerped -= Time.deltaTime / 0.35f;
        spectrumLerped = Mathf.Clamp(spectrumLerped, 0.0f, 0.2f);

        float output = (indicateNum) + spectrumLerped;
        the_renderer.material.SetFloat("_RANGE", output);
    }

    
}
