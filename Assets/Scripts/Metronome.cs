﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Experimental.Rendering.HDPipeline;
public class Metronome : MonoBehaviour
{
    // 之後用scriptable object + NoteAttribute存歌的屬性比較好 // 改放在sobject裡面這邊參數可以簡化了
    //public AudioSource theMainSongAudioSource;
    public float lastBeat = 0;     // 音樂如果比較晚開始，就讓這值是負值(加入offset的意思)
    public float quarterNoteTime;   // 音樂的解析度:每x秒算一次
    public int totalBeatCount;
    //public float songOffset;

    //public Songs nowplay; //這樣會抱錯 難道真的要scriptable然後給另一個scriptable繼承?
    [SerializeField]
    public GameLevelMusic nowplay; //幹真的要這樣 // 要改誰生成這顆譜的話請到ForGameMusic 
    public AudioSource LevelAudioSource;

    public static float arriveTime = 0.8f; // 音符提早生成的時間(音符到目標所花時間) 不要設超過StartDelay // 現在設成EletricEffect上的音符到達時間(一訂要"相對") 因為飛行速度跟提早生成的時間彼此在程式上沒有關聯
    // 自訂事件
    public event System.EventHandler<NotesEventArgs> SpawnNote;
    public event System.EventHandler<NotesEventArgs> SpawnLaser;
    public event System.EventHandler<NotesEventArgs> TimeToPress;

    int spawnLoopStartNum = 0;
    int ReceiveLoopStartNum = 0;

    // 給延遲啟動用的
    public float StartDelay = 3;
    private float StartDelayTimer = 0;
    bool IsPlaying = false;


    // 節拍輸入的容許誤差
    public float tolerence = 0;
    public float offset = 0; // 整體偏移 避免全都太快或太慢
    private float playtime = 0;
    // 分數
    public static int score = 0;
    public static int combo = 0;
    public static int bad = 0;
    public static int good = 0;
    public static int miss = 0;
    public Text b;

    // 頻譜分析
    public float[] spectrum;

    // 大絕計量
    float totalNote;
    public static float each_note_Ultimate_count;
    public static float nowUltimateNum = 0;
    public TextMeshPro comboTextmesh;

    // 拿異次元的材質球sample
    public Material retardedTunnelrenderer;
    public float tunneBrightness = 0;
    public bool shouldBright = false;
    public float tunnelBrightTime;
    public float tunnelBright_TimeR;
    float lerped = 0;

    // 換場 結尾
    Image fadingImage;
    public NumParseGuy numParseGuy;
    public Player player;
    public Animator bossAnimator;
    public Volume sceneVolumn;
    public HDRISky hdri_Sky;
    void Awake()
    {
        
        try
        {
            sceneVolumn.profile.TryGet(out hdri_Sky);
            hdri_Sky.hdriSky.value = GameObject.FindGameObjectWithTag("scriptableSavior").GetComponent<GotoMain>().cubeMap.value;
        }
        catch
        {
            Debug.Log("沒有GotoMain");
        }


        nowplay = this.GetComponent<GameLevelMusic>();
       // nowplay.noteJsonFileName = GameObject.FindGameObjectWithTag("scriptableSavior").GetComponent<GotoMain>().jsonFileName;
        nowplay.GameLevelMusicInstantiate(); // 如果不能用constructor只好先改成這樣
        spectrum = new float[256];

        LevelAudioSource = GameObject.FindGameObjectWithTag("LevelAudioSource").GetComponent<AudioSource>();
        LevelAudioSource.clip = nowplay.audio;

        // 每顆音符的大招計量
        nowUltimateNum = 0.05f;
        good = 0;
        bad = 0;
        miss = 0;
        totalNote = nowplay.notes.Count;
        each_note_Ultimate_count = 1 / totalNote + 0.0005f;
        string temp = each_note_Ultimate_count.ToString("0.####");
        each_note_Ultimate_count = float.Parse(temp);
        Debug.Log(each_note_Ultimate_count + "大招計量");

        DontDestroyOnLoad(numParseGuy);

        try
        {
            fadingImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
            // Fade效果
            fadingImage.gameObject.GetComponent<Animator>().SetTrigger("FadeIn");
        }
        catch {
            Debug.Log("沒有fadingimage");
        }
        

        // 預防scriptableobject不見
        /*if(nowplay.audio == null)
        {
            nowplay = GameObject.FindGameObjectWithTag("scriptableSavior").GetComponent<GotoMain>().nowplay;
        }*/
    }

    // 之後寫別的地方
    void Start()
    {
        
    }

    // 之後寫別的地方
    void Update()  /////////////////////////////////////////////////////////注意 原本是fixed update 我把它改成 update 不然shader會有問題
    {
        // 進結算的測試
        /*if (Input.GetKeyDown(KeyCode.J))
        {
            //nowUltimateNum = 0.81f;
            StartCoroutine(IEndGame());

        }*/

        Play();
        CallPressing(tolerence); // 之後tolerence可能寫在譜里比較好

        comboTextmesh.text = combo.ToString("000");

        GiveItToMe();

        if(nowUltimateNum <= 0)
        {
            nowUltimateNum = 0;
        }

        // 頻譜分析
        //float[] spectrum = new float[256];
        AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
        for (int i = 0; i < spectrum.Length; i++)
        {
            if (LevelAudioSource.volume > 0.02f)
                spectrum[i] /= LevelAudioSource.volume - 0.02f;
        }

        // 以下是trahstunnel的code////////////////////////////////////
        if (tunneBrightness > 0.2)
        {
            tunneBrightness -= Time.deltaTime * 3.0f;
        }
        else
        {
            tunneBrightness = 0.2f;
        }
        if (tunnelBright_TimeR > 0)
        {
            tunnelBright_TimeR -= Time.deltaTime;
        }
        else
        {
            tunnelBright_TimeR = 0;
            shouldBright = true;
        }

        if (IsPlaying && shouldBright)
        {
            tunneBrightness = 1;
            shouldBright = false;
            tunnelBright_TimeR = tunnelBrightTime;
        }
        // 以上是trahstunnel的code////////////////////////////////////

        lerped = Mathf.Lerp(lerped, tunneBrightness, Time.deltaTime);
        retardedTunnelrenderer.SetFloat("Vector1_E4188583", tunneBrightness);

        if (this.SpawnNote != null)
        {
            SummonNotes();
        }
        b.text = score.ToString();

        if (Input.GetKey(KeyCode.F1) && Input.GetKey(KeyCode.F2) && Input.GetKeyDown(KeyCode.UpArrow))
        {
            LevelAudioSource.volume += 0.01f;
        }
        else if (Input.GetKey(KeyCode.F1) && Input.GetKey(KeyCode.F2) && Input.GetKeyDown(KeyCode.DownArrow))
        {
            LevelAudioSource.volume -= 0.01f;
        }

    }


    public void SummonNotes()
    {
        
        if (LevelAudioSource.time <= 0 && playtime - StartDelay >= (nowplay.notes[spawnLoopStartNum].Appear_Time - arriveTime + offset)&& spawnLoopStartNum < nowplay.notes.Count - 1) // 這樣寫不對
        {
            SpawnNote?.Invoke(this, new NotesEventArgs(nowplay.notes[spawnLoopStartNum], nowplay.notes[spawnLoopStartNum].spawnners)); // 誰生成的應該在讀譜的時候就先記錄好吧 // 完成
            spawnLoopStartNum++;
        }
        else if (LevelAudioSource.time > 0 && LevelAudioSource.time >= (nowplay.notes[spawnLoopStartNum].Appear_Time - (arriveTime ) + offset) && spawnLoopStartNum < nowplay.notes.Count - 1)
        {
            //Debug.Log("Type >> " + nowplay.notes[spawnLoopStartNum].attacktype[0]);
            if ((nowplay.notes[spawnLoopStartNum].attacktype[0] == "Laser"))
            {
                string[] laserinput = nowplay.notes[spawnLoopStartNum].column1.Split('|');
                float[] laserinput_float = new float[5];
                for(int i = 0 ; i < laserinput.Length; i++)
                {
                    laserinput_float[i] = float.Parse(laserinput[i]);
                }
                SpawnLaser?.Invoke(this, new NotesEventArgs(laserinput_float[0], int.Parse(laserinput_float[1].ToString()), laserinput_float[2], laserinput_float[3], laserinput_float[4]));
                spawnLoopStartNum++;
            }
            else
            {
                SpawnNote?.Invoke(this, new NotesEventArgs(nowplay.notes[spawnLoopStartNum], nowplay.notes[spawnLoopStartNum].spawnners));
                spawnLoopStartNum++;
                //tunneBrightness = 1;
                
            }

        }
        else if(spawnLoopStartNum == nowplay.notes.Count - 1)
        {
            StartCoroutine(IEndGame());
        }

    }
    // 這個跟上面的方法分太開 很難傳譜的實例 // 最好一開始就傳完資料到個別按鍵  
    // 或是把判斷誰要開threshold的參數在傳送前就整理好，在飛的譜雖然是準的但跟按鍵後台判斷實際上要很分開
    public void CallPressing(float _tolerence) 
    {
        if (LevelAudioSource.time <= 0 && playtime - StartDelay >= (nowplay.notes[ReceiveLoopStartNum].Appear_Time + offset) && ReceiveLoopStartNum < nowplay.notes.Count - 1)
        {
            float thisnoteTime = nowplay.notes[ReceiveLoopStartNum].Appear_Time + offset;
            float nextNoteTime = thisnoteTime;
            if (nowplay.notes[ReceiveLoopStartNum].spawnners == nowplay.notes[ReceiveLoopStartNum + 1].spawnners)
            {
                nextNoteTime = nowplay.notes[ReceiveLoopStartNum + 1].Appear_Time + offset;
            }
            
            TimeToPress?.Invoke(this, new NotesEventArgs(_tolerence, nowplay.notes[ReceiveLoopStartNum].spawnners, nextNoteTime - thisnoteTime));
            ReceiveLoopStartNum++;
            
            
        }
        else if (LevelAudioSource.time > 0 && LevelAudioSource.time >= (nowplay.notes[ReceiveLoopStartNum].Appear_Time - _tolerence + offset) && ReceiveLoopStartNum < nowplay.notes.Count - 1)
        {
            float thisnoteTime = nowplay.notes[ReceiveLoopStartNum].Appear_Time + offset - _tolerence;
            float nextNoteTime = 0;

            

            for(int i = 0; i < nowplay.notes.Count; i++) // 這邊計算太多了其實 要注意
            {
                if (nowplay.notes[ReceiveLoopStartNum].spawnners[0] == true && nowplay.notes[ReceiveLoopStartNum].spawnners[0] == nowplay.notes[i].spawnners[0])
                {
                    nextNoteTime = nowplay.notes[ReceiveLoopStartNum + i].Appear_Time + offset - _tolerence;
                   // Debug.Log("0 is true");
                    i = nowplay.notes.Count;
                }
                else if (nowplay.notes[ReceiveLoopStartNum].spawnners[1] == true && nowplay.notes[ReceiveLoopStartNum].spawnners[1] == nowplay.notes[i].spawnners[1])
                {
                    nextNoteTime = nowplay.notes[ReceiveLoopStartNum + i].Appear_Time + offset - _tolerence;
                   // Debug.Log("1 is true");
                    i = nowplay.notes.Count;
                }

            }
            
            //Debug.Log(nextNoteTime - thisnoteTime + "NEXTNOTETIME");
            TimeToPress?.Invoke(this, new NotesEventArgs(_tolerence, nowplay.notes[ReceiveLoopStartNum].spawnners, nextNoteTime - thisnoteTime));
            ReceiveLoopStartNum++;
            
        }
    }


    public void Play()
    {
        playtime = StartDelayTimer + LevelAudioSource.time; // 
        if (StartDelayTimer < StartDelay - 0.021f && IsPlaying == false) // 0.021大約是fixedupdate一幀的時間 防止他扣成負數
        {
            StartDelayTimer += Time.deltaTime;
        }
        else if (StartDelayTimer >= StartDelay - 0.021f && IsPlaying == false)
        {
            StartDelayTimer = StartDelay;
            LevelAudioSource.Play();
            IsPlaying = true;
        }
    }
    /*
    public void EndGame()
    {
        fadingImage.GetComponent<Animator>().SetTrigger("FadeOut");
        SceneManager.LoadScene("Result");

    }*/

    IEnumerator IEndGame()
    {
       // Debug.Log("大絕計量 = " + nowUltimateNum);
        DontDestroyOnLoad(fadingImage.transform.parent.gameObject);
        if (nowUltimateNum >= 0.5f)
        {
            player.LaserUltimate();
            bossAnimator.SetTrigger("BOSS_DIE");
            // Boss死亡
            yield return new WaitForSeconds(6.0f);

            GetComponent<Fly>().setflySpeed = 50.0f;
            bossAnimator.gameObject.GetComponent<Rigidbody>().useGravity = true;
            /*yield return new WaitForSeconds(7.0f);
            bossAnimator.gameObject.transform.parent = null;*/


            yield return new WaitForSeconds(1.4f);

        }
        else
        {
            //player.gameObject.transform.parent = null;
            GetComponent<Fly>().setflySpeed = 30.0f;
            int temp = 0;

            while (temp < 200)
            {
                bossAnimator.gameObject.transform.position = new Vector3(bossAnimator.gameObject.transform.position.x, bossAnimator.gameObject.transform.position.y + 1, bossAnimator.gameObject.transform.position.z + 0.2f);
                yield return new WaitForSeconds(0.01f);
                temp++;
            }


        }
        Debug.Log(LevelAudioSource.volume + "audioVolume");
        while (LevelAudioSource.volume > 0.005f)
        {
            LevelAudioSource.volume -= 0.0005f;
            Debug.Log(LevelAudioSource.volume);
            yield return new WaitForSeconds(0.1f);
        }

        fadingImage.GetComponent<Animator>().SetTrigger("FadeOut");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("Result");
    }

    public void GiveItToMe()
    {
        numParseGuy.perfectNum = good;
        numParseGuy.goodNum = bad;
        numParseGuy.missNum = miss;
        numParseGuy.UltimateNum = nowUltimateNum;
        if(nowUltimateNum >= 0.5f)
        {
            numParseGuy.winOrLose = true;
        }
        else
        {
            numParseGuy.winOrLose = false;
        }
    }
}
