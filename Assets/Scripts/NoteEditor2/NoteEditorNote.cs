﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// 每個note都掛一個這個 目前很多數字的部分很寫死
// 之後發通知給controller更新串列

// 如果boxcollider要保持在左邊緣的話 中心點x要是x長度的一半
[RequireComponent(typeof(InputField))]
public class NoteEditorNote : MonoBehaviour
{
    [HideInInspector]
    public float deltaX, deltaY;
    [HideInInspector]
    public float lastMoveEndPosx, lastMoveEndPosy;
    Vector2 mousePosition;
    [HideInInspector]
    public float note_clamp_x, note_clamp_y;

    LineRenderer lineRenderer;
    NoteEditorController gameController;
    [HideInInspector]
    public int this_Selected = -1; //初始設定-1來判斷他出生後有沒有被移動過

    public event System.EventHandler<NoteEditorEventArgs> CallMovetTgetherDEL;
    [HideInInspector]
    public Vector2 OriginPosBeforeDrag, TotalMovedOnLastDrag;

    public LineRenderer Appearence;
    [Header("後臺屬性")]
    public GameObject AttributePanel;
    public int aaaaa;

    public float measurement, Measure32;
    private Dropdown attacktypeDropDown;
    public string[] attackType = new string[] { };
    public string laserAttribute;
    [HideInInspector]
    public Color currentColor;

    private InputField measurement_InputField, measure32_InputField;
    public int rowPos = 0;
    public InputField column1;
    public InputField column2;
    public InputField column3;

    public float posx, posy; // 用來記錄位置 因為遊戲中的positon容易跑掉

    void Awake()
    {
        
        attackType = new string[1]; // 重設一下attacktype(宣告在執行範圍外的變數不會更新) // 暫時設1 之後如果真有multiple型態在想辦法

        lineRenderer = GameObject.FindGameObjectWithTag("LineRenderer").GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<NoteEditorController>();
        SignGameControllerEvent(true);
        
        lastMoveEndPosx = transform.position.x;
        lastMoveEndPosy = transform.position.y;

        measurement_InputField = GameObject.FindGameObjectWithTag("measurement_inputfield").GetComponent<InputField>();
        measure32_InputField = GameObject.FindGameObjectWithTag("measurement32_inputfield").GetComponent<InputField>();

        // 設定攻擊模式
        this_Selected = 1;// 先打開 attributepanel不然不能access
        AttributePanelToggle(this, System.EventArgs.Empty); // 先打開 attributepanel不然不能access

        if(this.GetComponentInChildren<Dropdown>().gameObject.tag == "AttackType") // disable中的dropdown會抓不到 注意
        {
            attacktypeDropDown = this.GetComponentInChildren<Dropdown>();
        }
        attackType[0] = attacktypeDropDown.options[attacktypeDropDown.value].text; // 有夠迂迴的拿dropdown選取的text

        this_Selected = 0;
        AttributePanelToggle(this, System.EventArgs.Empty);// 關閉panel

        // 選攻擊模式的dropdown ，value變了就叫一下SetAttackType
        attacktypeDropDown.onValueChanged.AddListener(delegate { SetAttackType(); });

        currentColor = gameController.noteVarius_01_Color ;
    }

    //很不想在note上跑但是沒時間 先這樣寫
    void Update()
    {
        laserAttribute = column1.text; //-2.48
        float posytemp = transform.position.y;
        rowPos = (int)(Mathf.Abs(((((posytemp + 2.48f) / 0.9f - 0.001f))))); // 硬算的
        

    }
    // down drag up 改成射線判斷? 或是至少按在上面的時候就不會觸發noteeditorController裡面的getkey
    // 已改成只有位置判斷 沒有更改參數
    private void OnMouseDown()
    {
        

        deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
        deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;

        OriginPosBeforeDrag = transform.position;

        //Update_MeasureNum();
    }

    private void OnMouseDrag()
    {
        // 以下限制跟移動音符
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePosition.x - deltaX, mousePosition.y - deltaY);

        // 定義拖拉的邊界
        note_clamp_x = gameController.pixelPerMeasurement * (int)(transform.position.x / gameController.pixelPerMeasurement);
        note_clamp_x = Mathf.Clamp(note_clamp_x, 0, gameController.measureNum * gameController.measureSpacing - gameController.pixelPerMeasurement); // pixelPerMeasurement是一小格的距離

        // y的最低點是row總數減掉1再乘上row的間隔 而最高點就是2.7 第一條線
        note_clamp_y = transform.position.y;

        // 下面算式基本上就是硬算的 gameController.noteOnRowOffset是測試出來的間隔 0.3f是因為pivot在左上不是中間
        note_clamp_y = (gameController.rowStartYPos + gameController.rowSpace * (int)((transform.position.y - gameController.rowStartYPos) / gameController.rowSpace)) - gameController.noteOnRowOffset;
        note_clamp_y = Mathf.Clamp(
            note_clamp_y,
            gameController.rowStartYPos + (gameController.rowSpace * (gameController.rowNum - 1) - gameController.noteOnRowOffset),
            gameController.rowStartYPos - gameController.noteOnRowOffset);

      
        TotalMovedOnLastDrag = (Vector2)OriginPosBeforeDrag - new Vector2(note_clamp_x, note_clamp_y);
        CallMovetTgetherDEL -= gameController.EndMoveAllSelectedNote;
        CallMovetTgetherDEL += gameController.MoveAllSelectedNote;
        CallMovetTgetherDEL?.Invoke(this, new NoteEditorEventArgs(this));
        
        // 輔助線的部分
        // 0.6f是方塊大小用在Y軸 因為他是60像素 而且每個單位是100像素
        // 0.03f是讓線稍微前面一點跟方塊切齊
        Vector3 guideLineStartPos = new Vector3(transform.position.x + 0.03f, transform.position.y - 0.6f, transform.position.z);
        lineRenderer.SetPosition(0, guideLineStartPos);
        lineRenderer.SetPosition(1, new Vector3(guideLineStartPos.x, 0, guideLineStartPos.z));

        Update_MeasureNum();
    }

    private void OnMouseUp()
    {
        // 關閉輔助線
        lineRenderer.SetPosition(0, new Vector3(0, 0, 0));
        lineRenderer.SetPosition(1, new Vector3(0, 0, 0));

        // 放開滑鼠後黏到定義的grid中
        // 注意這裡(int)那段要可以是0才會是正確的offset(這樣起點就是rowStartYPos) 但因為起始點value不是0所以要 - gameController.rowStartYPos
        // 下面算式基本上就是硬算的 gameController.noteOnRowOffset是測試出來的間隔 0.3f是因為pivot在左上不是中間
        note_clamp_y = (gameController.rowStartYPos + gameController.rowSpace * (int)((transform.position.y - gameController.rowStartYPos) / gameController.rowSpace)) - gameController.noteOnRowOffset;
        //transform.position = new Vector2(note_clamp_x, note_clamp_y);
        CallMovetTgetherDEL += gameController.EndMoveAllSelectedNote;
        CallMovetTgetherDEL -= gameController.MoveAllSelectedNote;
        CallMovetTgetherDEL?.Invoke(this, new NoteEditorEventArgs(this));

        Update_MeasureNum();
        ResetOriginalPos();
    }

    // 這邊接收controller的通知 如果args傳入的物件是自己 就當作是被選取並改顏色
    public void Click_Listener(object sender, NoteEditorEventArgs e)
    {
        bool hasFound = false;
        //Debug.Log(e.gameobjs.Count);
        if (e.gameobjs.Count == 0 && this_Selected != -1) // 如果傳來的是0長度的list 就當作是沒選到任何東西
        {
            this_Selected = 0;
            Appearence.material.color = currentColor;// unselectedcolor被我改成currentcolor(為了配合改這顆音符的attacktype 避免一植被selected蓋掉
        }
        else
        {
            for (int i = 0; i < e.gameobjs.Count; i++) 
            {
                if (e.gameobjs[i] == this.gameObject) // 這個用NoteEditorNote是不是會比較省資源
                {
                    this_Selected = 1;
                    hasFound = true;
                    Appearence.material.color = gameController.noteSelectedColor; 
                    
                }
                else if (e.gameobjs[i] != this.gameObject && this_Selected != -1 && hasFound == false)//-1代表他還沒被碰過 該是沒碰過的顏色
                {
                    this_Selected = 0;
                    Appearence.material.color = currentColor;
                    
                }
            }
        }
        

    }
    
    public void ResetOriginalPos()
    {
        lastMoveEndPosx = transform.position.x;
        lastMoveEndPosy = transform.position.y;
    }

    public void AttributePanelToggle(object sender, System.EventArgs e)
    {
        if(AttributePanel.activeInHierarchy == false && this_Selected == 1)
        {
            AttributePanel.SetActive(true);
        }
        else if(AttributePanel.activeInHierarchy == true && this_Selected != 1)
        {
            AttributePanel.SetActive(false);
        }
        
    }

    public void SignGameControllerEvent(bool b)
    {
        if (b == true)
        {
            gameController.MyOnMouseClick += this.Click_Listener;
            gameController.PressedWEvent += this.AttributePanelToggle;
        }
        else
        {
            gameController.MyOnMouseClick -= this.Click_Listener;
            gameController.PressedWEvent -= this.AttributePanelToggle;
        }
    }

    public void Update_MeasureNum()
    {
        measurement = ((int)(transform.position.x / gameController.pixelPerMeasurement) / (int)128) + 1; // 一小節共128格
        Measure32 = ((transform.position.x % 19.2f) / gameController.pixelPerMeasurement); // 19.2f是一小節的長度
     
        // 做一個四捨五入不然會有0.99999甚麼的
        if(Measure32 % 1 != 0)
        {
            Measure32 = Measure32 % 1 > 0.5f ? (int)Measure32 + 1 : (int)Measure32;
        }

        measurement_InputField.text = measurement.ToString();
        measure32_InputField.text = Measure32.ToString();
        //Debug.Log(Measure32);
    }
    public void Update_Position(int h, int seg, int seg32)
    {
        seg32 = seg32 == -1 || seg32 > 127 ? (int)Measure32 : seg32; // 如果我傳-1給他代表不要改
        seg = seg == -1 || seg > gameController.measureNum ? (int)measurement : seg;
        float dis = ((seg - 1) * 19.2f + seg32 * gameController.pixelPerMeasurement )- transform.position.x;

        transform.position = new Vector2(
            transform.position.x + dis,
            transform.position.y
            );

        // 記得也更新一下每粒譜自己的資料
        Update_MeasureNum();
    }

    public void SetRealPosition(float a, float b)
    {
        posx = a;
        posy = b;
    }

    public void SetAttackType()
    {
        attackType[0] = attacktypeDropDown.options[attacktypeDropDown.value].text;
        if(attacktypeDropDown.value == 0)
        {
            currentColor = gameController.noteVarius_01_Color;
            Appearence.material.color = currentColor;
        }
        else if(attacktypeDropDown.value == 1) // 雷射
        {
            currentColor = gameController.noteVarius_02_Color;
            Appearence.material.color = currentColor; 

        }
        
        
    }

    public void AttacktypeUpdate() 
    {
        for(int i = 0; i < attacktypeDropDown.options.Count;i++)
        {

            if (attackType[0] == attacktypeDropDown.options[i].text)
            {
                attacktypeDropDown.value = i;
            }
        }
       

    }
}
