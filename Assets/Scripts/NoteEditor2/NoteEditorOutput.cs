﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System; // 這行要加不然不能用Serializable
using Newtonsoft.Json; // 插件json處理

[Serializable]
public class NoteData 
{
    
    public Output[] everyNotes; // 注意這裡命名一定要是everyNotes 為了配合解讀的程式
    
    //public int[] why = new int[] { 1,55,66,77,88};

    
}

[Serializable]
public struct Output
{
    public int id;
    public int measurement;
    public int measure32;
    public string[] attacktype;
    public int rowPos;
    public string column1;
    public string column2;
    public string column3;
}

[Serializable]
public class NoteEditorOutput : MonoBehaviour
{

    public NoteData _notedata;

    // 用來把notes(gameobject)裡面的屬性拆開方便存入Notedata
    public Output[] _outputArrayS;

    public List<GameObject> allnote_ForOutput;
    
    void Start()
    {
        
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKey(KeyCode.RightControl))
        {
            OutputJson();
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightControl))
        {
            InputJson();
        }
    }

    // 還沒寫完20200430
    void OutputJson()
    {
        // 先蒐集所有音符
        GameObject[] FindNotes = GameObject.FindGameObjectsWithTag("notes");
        

        // 排序
        for(int i = 0; i < FindNotes.Length - 1; i++)
        {
            for (int a = i + 1; a < FindNotes.Length; a++)
            {
                NoteEditorNote temp = FindNotes[i].GetComponent<NoteEditorNote>();
                int position = (int)(temp.measurement) * 127 + (int)temp.Measure32;
                NoteEditorNote temp_next = FindNotes[a].GetComponent<NoteEditorNote>();
                int position_next = (int)(temp_next.measurement) * 127 + (int)temp_next.Measure32;

                if (position >= position_next)
                {
                    GameObject temp_swap = FindNotes[i];
                    FindNotes[i] = FindNotes[a];
                    FindNotes[a] = temp_swap;
                }
            }
        }

        List<Output> _outputArrayS_list_temp = new List<Output>();
        for (int i = 0; i < FindNotes.Length; i++)
        {
            // 先把findnotes[i]拆出來放到一個temp
            // 這裡寫入每個音符屬性
            Output temp = new Output();
            temp.id = i;
            temp.measurement = (int)FindNotes[i].GetComponent<NoteEditorNote>().measurement;
            temp.measure32 = (int)FindNotes[i].GetComponent<NoteEditorNote>().Measure32;
            temp.rowPos = FindNotes[i].GetComponent<NoteEditorNote>().rowPos;
            temp.attacktype = FindNotes[i].GetComponent<NoteEditorNote>().attackType;
            temp.column1 = FindNotes[i].GetComponent<NoteEditorNote>().column1.text;
            temp.column2 = FindNotes[i].GetComponent<NoteEditorNote>().column2.text;
            temp.column3= FindNotes[i].GetComponent<NoteEditorNote>().column3.text;
            if (temp.attacktype[0] == "Laser")
            {
                temp.column1 = FindNotes[i].GetComponent<NoteEditorNote>().laserAttribute;
            }



            // 每個temp存回outputarray陣列
            _outputArrayS_list_temp.Add(temp);
        }

        _outputArrayS = new Output[FindNotes.Length];
        for (int i = 0; i < FindNotes.Length; i++)
        {
            _outputArrayS[i] = _outputArrayS_list_temp[i];
        }

        Debug.Log(_outputArrayS.Length + "whynotoutput");


        // 只是在寫code的時候檢查對不對
        foreach (GameObject a in FindNotes)
        {
            Debug.Log(a.GetComponent<NoteEditorNote>().measurement + " << >>" + a.GetComponent<NoteEditorNote>().Measure32);
        }

        // 把自己重新組合好的struct丟給json 才能印要的屬性
        _notedata.everyNotes = _outputArrayS;
        
        string jsonInfo = JsonConvert.SerializeObject(_notedata, Formatting.Indented); // newtonsoft json
        File.WriteAllText("Y:/TemporeniowO/NoteEditorOutput/testOutput_ReallyEasy.json", jsonInfo);

        Debug.Log("寫入完成");

       
    }

    NoteData ndata;
    public GameObject notePrefab;
    void InputJson()
    {
        string jsoninfo;
        StreamReader file = File.OpenText(@"Y:/TemporeniowO/NoteEditorOutput/testOutput_ReallyEasy.json");
        JsonSerializer serializer = new JsonSerializer();
        
        ndata = (NoteData)serializer.Deserialize(file, typeof(NoteData));
        jsoninfo = File.ReadAllText("Y:/TemporeniowO/NoteEditorOutput/testOutput_ReallyEasy.json");
        file.Close();
        //Debug.Log(ndata.everyNotes[3].attacktype[0]);

        Output[] allnote = ndata.everyNotes;
        for (int i = 0; i < ndata.everyNotes.Length; i++)
        {
            GameObject gametemp = Instantiate(notePrefab);
            NoteEditorNote tempnote = gametemp.GetComponent<NoteEditorNote>();
            gametemp.transform.position = new Vector3(((allnote[i].measurement - 1)* 128 + allnote[i].measure32) * 0.15f, 0 - (((allnote[i].rowPos) * 0.9f) + 2.48f), 0);
            tempnote.attackType = allnote[i].attacktype; // 需要一個東西是從內部參數更新ui 
            tempnote.measurement = allnote[i].measurement;
            tempnote.Measure32 = allnote[i].measure32;
            tempnote.column1.text = allnote[i].column1;
            tempnote.column2.text = allnote[i].column2;
            tempnote.column3.text = allnote[i].column3;
            tempnote.AttacktypeUpdate();
            tempnote.OriginPosBeforeDrag = tempnote.gameObject.transform.position;
            this.gameObject.GetComponent<NoteEditorController>().ValidCheck();
            this.gameObject.GetComponent<NoteEditorController>().AdjustNotes();
        }
    }
}
