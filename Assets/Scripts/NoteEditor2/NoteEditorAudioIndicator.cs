﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteEditorAudioIndicator : MonoBehaviour
{
    NoteEditorController gameController;
    private int this_Selected = 0;
    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<NoteEditorController>();
        gameController.MyOnMouseClick += Click_Listener;
    }

    
    void Update()
    {
        
    
    }
    
    
    public void Click_Listener(object sender, NoteEditorEventArgs e)
    {
        bool hasFound = false;
        //Debug.Log(e.gameobjs.Count);
        if (e.gameobjs.Count == 0 && this_Selected != -1) // 如果傳來的是0長度的list 就當作是沒選到任何東西
        {
            this_Selected = 0;
            
        }
        else
        {
            for (int i = 0; i < e.gameobjs.Count; i++)
            {
                if (e.gameobjs[i] == this.gameObject) // 這個用NoteEditorNote是不是會比較省資源
                {
                    this_Selected = 1;
                    hasFound = true;
                    Debug.Log("選到音樂標線");


                }
                else if (e.gameobjs[i] != this.gameObject && this_Selected != -1 && hasFound == false)//-1代表他還沒被碰過 該是沒碰過的顏色
                {
                    this_Selected = 0;
             

                }
            }
        }


    }

    // 以下從noteeditornote拿的
    [HideInInspector]
    public float deltaX, deltaY;
    [HideInInspector]
    public float lastMoveEndPosx, lastMoveEndPosy;
    Vector2 mousePosition;
    [HideInInspector]
    public float note_clamp_x, note_clamp_y;
    [HideInInspector]
    public Vector2 OriginPosBeforeDrag, TotalMovedOnLastDrag;
   
    private void OnMouseDown()
    {


        deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
        deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;

        OriginPosBeforeDrag = transform.position;

        //Update_MeasureNum();
    }

    private void OnMouseDrag()
    {
        // 以下限制跟移動音符
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePosition.x - deltaX, /*mousePosition.y - deltaY*/0);

        // 定義拖拉的邊界
        note_clamp_x = gameController.pixelPerMeasurement * (int)(transform.position.x / gameController.pixelPerMeasurement);
        note_clamp_x = Mathf.Clamp(note_clamp_x, 0, gameController.measureNum * gameController.measureSpacing - gameController.pixelPerMeasurement); // pixelPerMeasurement是一小格的距離

        // y的最低點是row總數減掉1再乘上row的間隔 而最高點就是2.7 第一條線
        note_clamp_y = transform.position.y;

        // 下面算式基本上就是硬算的 gameController.noteOnRowOffset是測試出來的間隔 0.3f是因為pivot在左上不是中間
        note_clamp_y = (gameController.rowStartYPos + gameController.rowSpace * (int)((transform.position.y - gameController.rowStartYPos) / gameController.rowSpace)) - gameController.noteOnRowOffset;
        note_clamp_y = Mathf.Clamp(
            note_clamp_y,
            gameController.rowStartYPos + (gameController.rowSpace * (gameController.rowNum - 1) - gameController.noteOnRowOffset),
            gameController.rowStartYPos - gameController.noteOnRowOffset);


        TotalMovedOnLastDrag = (Vector2)OriginPosBeforeDrag - new Vector2(note_clamp_x, note_clamp_y);




        transform.position = new Vector2(note_clamp_x, /*mousePosition.y - deltaY*/0);

    }

    private void OnMouseUp()
    {
        

        // 放開滑鼠後黏到定義的grid中
        // 注意這裡(int)那段要可以是0才會是正確的offset(這樣起點就是rowStartYPos) 但因為起始點value不是0所以要 - gameController.rowStartYPos
        // 下面算式基本上就是硬算的 gameController.noteOnRowOffset是測試出來的間隔 0.3f是因為pivot在左上不是中間
        note_clamp_y = (gameController.rowStartYPos + gameController.rowSpace * (int)((transform.position.y - gameController.rowStartYPos) / gameController.rowSpace)) - gameController.noteOnRowOffset;
        //transform.position = new Vector2(note_clamp_x, note_clamp_y);
     

        
        ResetOriginalPos();
    }
    public void ResetOriginalPos()
    {
        lastMoveEndPosx = transform.position.x;
        lastMoveEndPosy = transform.position.y;
    }
}
