﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 這是框選時會啟動的文件 主要是靠一個collider判斷有沒有選到
public class NoteEditorSelectBox : MonoBehaviour
{
    NoteEditorController gamecontroller;

    public delegate void BoxSelectingDel(Collider other);
    public BoxSelectingDel boxSelect_ExitDEL;
    public BoxSelectingDel boxSelect_EnterDEL;

    void Awake()
    {
        gamecontroller = GameObject.FindGameObjectWithTag("GameController").GetComponent<NoteEditorController>();

    }

    private void OnTriggerEnter(Collider other)
    {
        boxSelect_EnterDEL?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {

        boxSelect_ExitDEL?.Invoke(other);


    }

    // 用來移除原本在框選範圍內但後來在框選範圍外的音符 平常會放進一個委派 某些情況會移除委派
    public void RemoveOutsideBoxNote(Collider _other)
    {
        if (_other.gameObject.GetComponent<NoteEditorNote>() != null)
        {
            gamecontroller.selectedNotelist.Remove(_other.gameObject);
        }
    }

    public void AddInsideBoxNote(Collider _other)
    {
        //Debug.Log("啟動窗選");
        if (_other.gameObject.GetComponent<NoteEditorNote>() != null) // 這樣會跟單選的動作重複 變成按一下會連續加入兩次同個物件 // 可能在判定單點跟窗選那邊要寫分明一點 這樣很硬來
        {
            if (!gamecontroller.selectedNotelist.Contains(_other.gameObject))
            {
                gamecontroller.selectedNotelist.Add(_other.gameObject); // add是不是不該在這裡實作
            }
        }
    }
}
