﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteEdotorPanCamera : MonoBehaviour
{
    Vector3 lastPosition;
    public float mouseSensitivity = 0.1f;
    public InputField CameraPos_inputfield;

    public NoteEditorController gamecontroller;
    void Start()
    {
        CameraPos_inputfield.onEndEdit.AddListener(delegate { GotoPosition(CameraPos_inputfield); });
        gamecontroller = GameObject.FindGameObjectWithTag("GameController").GetComponent<NoteEditorController>();
    }
    void Update()
    {
        PanCamera();
        ResizeCamera();
        //Debug.Log(this.GetComponent<Camera>().orthographicSize);
    }

    public void PanCamera()
    {
        
        if (Input.GetMouseButtonDown(2))
        {
            lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(2))
        {
            Vector3 delta= lastPosition - Input.mousePosition;
            transform.Translate(delta.x * mouseSensitivity, delta.y * mouseSensitivity, 0);
            lastPosition = Input.mousePosition;
        }
    }

    public void ResizeCamera()
    {
        
        GetComponent<Camera>().orthographicSize += -Input.mouseScrollDelta.y;
        if (this.GetComponent<Camera>().orthographicSize <= 1)
        {
            GetComponent<Camera>().orthographicSize = 1;
        }
    }

    public void GotoPosition(InputField a)
    {
        if (int.Parse(a.text) >= 1 && int.Parse(a.text) <= gamecontroller.measureNum)
        {
            transform.position = new Vector3((int.Parse(a.text) - 1) * gamecontroller.measureSpacing, transform.position.y, transform.position.z);
        }
    }
}
