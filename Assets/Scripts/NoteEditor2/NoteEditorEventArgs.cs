﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteEditorEventArgs : System.EventArgs
{
    public GameObject measurePrefab;
    public int measureNum = 1;
    public float measureSpacing = 0;
    public NoteEditorController noteEditorController;
    public List<GameObject> gameobjs;
    public NoteEditorNote noteEditorNote;

    public NoteEditorEventArgs(GameObject _measurePrefab, int _measureNum, float _measureSpacing)
    {
        measurePrefab = _measurePrefab;
        measureNum = _measureNum;
        measureSpacing = _measureSpacing;


    }

    public NoteEditorEventArgs(NoteEditorController _noteEditorController)
    {
        noteEditorController = _noteEditorController;
    }
    
    public NoteEditorEventArgs(List<GameObject> _gameObjects)
    {
        gameobjs = _gameObjects;
    }

    public NoteEditorEventArgs(NoteEditorNote n )
    {
        noteEditorNote = n;

    }
}
