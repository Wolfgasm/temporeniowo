﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System.IO;
using System; // 這行要加不然不能用Serializable

public class NoteEditorController : MonoBehaviour
{
    // 一格是幾pixel
    [Header("[一格是多少座標]")]
    public float pixelPerMeasurement = 0.15f;// 秒數就是 x座標/0.15 * 1/8的一拍時間(4/4節奏) 所以理論上是可以不受限於32的解析度

    [Header("[音符要離上面那條橫線多遠]")]
    // 音符要離上面那條橫線多遠
    public float noteOnRowOffset = 0.13f;

    [Header("[橫線]")]
    // 橫線
    public GameObject rowPrefab;
    public int rowNum = 1;
    public float rowSpace = 0.0f;
    public float rowStartYPos;

    [Header("[解析度格線]")]
    // 上面的格線
    public GameObject measurePrefab;
    public int measureNum = 1;
    public float measureSpacing = 0;

    [Header("[設定音符外觀]")]
    // 音符本體
    public GameObject note;
    public Color noteUnTouchedColor, noteMovedColor, noteSelectedColor, noteOverLappingColor, noteInvalidPosColor,
        noteVarius_01_Color, noteVarius_02_Color;

    //public event System.EventHandler<System.EventArgs> MyOnMouseDrag;
    public event System.EventHandler<NoteEditorEventArgs> MyOnMouseClick;
    [Header("[現在在編輯的歌]")]
    public AudioSource nowEditSong;
    public int BPM;
    public float startDelay;
    public GameObject musicIndicator;
    public Toggle previewToggle;
    public GameObject preveiewHitEffect;
    private float setDelayTimeTemp;
    bool indicatorCanStartGoing = false;
    float indicatorDelay = 0;
    [Header("[矩形選取]")]
    // 矩形選取用的
    public GameObject dragLineRenderer;
    Vector2 dragStartPoint, dragEndPoint;
    public BoxCollider selectBoxCollider;
    public delegate void BoxSelectDEL();
    BoxSelectDEL boxSelectDEL;
    
    // 存多選的物件
    public List<GameObject> selectedNotelist = new List<GameObject>();

    // 按下W的event(A是不是也要寫個)
    public event System.EventHandler<System.EventArgs> PressedWEvent;

    // 複製貼上用的
    public List<GameObject> allnotesinCopyUse;
    public List<Vector3> copyNotesVector = new List<Vector3>(); // 考量到noteeditornote也會帶資訊也得一併複製 可能不能單純存位置而已
    LineRenderer copylineRenderer;
    //
    public InputField Segment_inputfield, seg32_inputfield;
    void Awake()
    {
        Init();
        Segment_inputfield.onEndEdit.AddListener(delegate { AAAA(Segment_inputfield); }); // 在Inputfield上註冊事件 我猜inspector那裏跟這裡是一樣的
        seg32_inputfield.onEndEdit.AddListener(delegate { BBBB(seg32_inputfield); });
        previewToggle.onValueChanged.AddListener(delegate { SetPlayingSong(); });

        //Load();
        
    }

   
    
    void Update()
    {
        PressA();
        PressW();
        PressDel();
        PressO();
        PressI();
        PressC();
        PressV();
        Check_If_Selecting(); // 這裡一定要比Boxselect的委派還早執行 不然單擊會跑出框
        boxSelectDEL?.Invoke(); // 現在拖拉是不一定會clear了..
        TogglePreview();
        //Debug.Log(nowEditSong.time);

        //musicIndicator.transform.position = new Vector3(nowEditSong.time * (BPM / 60.0f) * (measureSpacing / 4.0f), 0, 0);
        //Camera.main.transform.position = new Vector3(musicIndicator.transform.position.x + 9.85f, Camera.main.transform.position.y, Camera.main.transform.position.z);
    }

    public void Init()
    {
        // 滑鼠拖拉的動作會觸發BoxSelect // 目前這邊判定有點鬆散
        //MyOnMouseDrag += BoxSelect;

        // 開始時先預設可以拖拉(因為開始一定還沒選音符)
        boxSelectDEL = BoxSelect;

        // 初始化窗選用的lineRendere
        dragLineRenderer.GetComponent<LineRenderer>().positionCount = 5; 

        // 貼上的輔助線
        copylineRenderer = GameObject.FindGameObjectWithTag("LineRenderer").GetComponent<LineRenderer>();

        // 生成row
        for (int j = 0; j <= rowNum; j++)
        {
            GameObject cloneRow = Instantiate(rowPrefab, transform); // linerenderer保持0.0.0就好
            LineRenderer cloneRowLineRenderer = cloneRow.GetComponent<LineRenderer>();
            cloneRowLineRenderer.positionCount = 2;
            cloneRowLineRenderer.SetPosition(0, new Vector2(0, rowStartYPos + (rowSpace * j)));
            cloneRowLineRenderer.SetPosition(1, new Vector2(measureNum * measureSpacing - pixelPerMeasurement, rowStartYPos + (rowSpace * j)));
        }
        // 生成時間軸
        for (int i = 0; i < measureNum; i++)
        {
            GameObject cloneMeasure = measurePrefab;
            if (i != 0)
            {
                cloneMeasure = Instantiate(measurePrefab);
                cloneMeasure.transform.position = new Vector3(0 + (i * measureSpacing),
                    cloneMeasure.transform.position.y,
                    cloneMeasure.transform.position.z);
            }
            cloneMeasure.GetComponent<NoteEditorMeasureLine>().thisSegmentNum = i;
            cloneMeasure.GetComponent<NoteEditorMeasureLine>().segmentNumText.text = (i + 1).ToString();
        }
    }

    public void PressA()
    {
        if (Input.GetKeyDown(KeyCode.A) && Input.GetKey(KeyCode.LeftControl)) // 如果按A有複數事情要做就要改event
        {
            CreateNotes();
        }
    }
    public void PressW()
    {
        if (Input.GetKeyDown(KeyCode.W) && Input.GetKey(KeyCode.LeftControl)) // 如果按A有複數事情要做就要改event
        {
            // 打開視窗
            PressedWEvent?.Invoke(this, System.EventArgs.Empty);
        }
    }
    public void PressDel()
    {
        if (Input.GetKeyDown(KeyCode.Delete) && Input.GetKey(KeyCode.RightControl)) // 如果按A有複數事情要做就要改event
        {
            //MyOnMouseClick?.Invoke(this, new NoteEditorEventArgs(new NoteEditorNote()));
            List<GameObject> temp = selectedNotelist;
            
            for (int i = 0; i < temp.Count; i++)
            {
                temp[i].GetComponent<NoteEditorNote>().SignGameControllerEvent(false); // 一定要記得反註冊不然Invoke會NULL
                Destroy(temp[i]);
            }
            selectedNotelist.Clear();
        }
    }

    public void PressO()
    {
        if (Input.GetKeyDown(KeyCode.O) /*&& Input.GetKey(KeyCode.RightShift)*/)
        {
            ValidCheck();
            Debug.Log("我明明按了O");
        }
        
    }

    public void PressI()
    {
        if (Input.GetKeyDown(KeyCode.I) /*&& Input.GetKey(KeyCode.RightShift)*/)
        {
            AdjustNotes();
        }
    }

    public void PressC()
    {
        if (Input.GetKeyDown(KeyCode.C) && Input.GetKey(KeyCode.LeftControl))
        {
            Copy();
        }
    }

    public void PressV()
    {
        if (Input.GetKey(KeyCode.V) && Input.GetKey(KeyCode.LeftControl))
        {
            CreateLaserGuideLine();
        }
        else if (Input.GetKeyUp(KeyCode.V) && Input.GetKey(KeyCode.LeftControl))
        {
            Paste();
        }
        else if (Input.GetKeyUp(KeyCode.V))
        {
            
        }
        
    }
    public void CreateNotes()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float note_clamp_x = pixelPerMeasurement * (int)(mousePosition.x / pixelPerMeasurement);
        float note_clamp_y = (rowStartYPos + rowSpace * (int)((mousePosition.y - rowStartYPos) / rowSpace)) - noteOnRowOffset;
        GameObject cloneNote = Instantiate(note, new Vector3(note_clamp_x, note_clamp_y, 0), transform.rotation);

        cloneNote.GetComponent<NoteEditorNote>().Appearence.material.color = noteUnTouchedColor;
    }

    // 這event args根本沒用到啊
    public void BoxSelect()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //Debug.Log("Get Clicked");

            // 開始滑動或單點時才啟動離開選取範圍會移除list的功能 // 這邊還是跟單擊寫一起吧 線在拉著音符拖動selectlist會直接不見
            selectBoxCollider.gameObject.GetComponent<NoteEditorSelectBox>().boxSelect_ExitDEL += 
                selectBoxCollider.gameObject.GetComponent<NoteEditorSelectBox>().RemoveOutsideBoxNote;
            selectBoxCollider.gameObject.GetComponent<NoteEditorSelectBox>().boxSelect_EnterDEL +=
                selectBoxCollider.gameObject.GetComponent<NoteEditorSelectBox>().AddInsideBoxNote;



            dragStartPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            dragEndPoint = dragStartPoint;

            // 先清空不然會紀錄上一次的位置
            for (int i = 0; i < dragLineRenderer.GetComponent<LineRenderer>().positionCount; i++)
            {
                dragLineRenderer.GetComponent<LineRenderer>().SetPosition(i, dragStartPoint);
            }

            dragLineRenderer.GetComponent<LineRenderer>().SetPosition(0, dragStartPoint);

            // 用一個矩形trigger蒐集碰到的物件
            // 按下時把起始點射得跟雷射一樣 去除z軸
            selectBoxCollider.gameObject.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            selectBoxCollider.gameObject.transform.position = new Vector3(selectBoxCollider.gameObject.transform.position.x,
                selectBoxCollider.gameObject.transform.position.y,
                0);
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            //Debug.Log("A");

            dragEndPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            dragLineRenderer.GetComponent<LineRenderer>().SetPosition(1, new Vector2(dragStartPoint.x, dragEndPoint.y));
            dragLineRenderer.GetComponent<LineRenderer>().SetPosition(2, new Vector2(dragEndPoint.x, dragEndPoint.y));
            dragLineRenderer.GetComponent<LineRenderer>().SetPosition(3, new Vector2(dragEndPoint.x, dragStartPoint.y));
            dragLineRenderer.GetComponent<LineRenderer>().SetPosition(4, new Vector2(dragStartPoint.x, dragStartPoint.y));

            // 矩形隨雷射的點變化
            // 矩形trigger的size定義:((|起點x| + |終點x|) , (|起點y| + |終點y|)) 然後center用起點終點向量相減套象限在/2 (套象限:終點-起點的向量後去判斷正負數)

            float selectbox_x = Vector3.Distance(new Vector3(dragEndPoint.x, 0, 0), new Vector3(dragStartPoint.x, 0, 0));
            float selectbox_y = Vector3.Distance(new Vector3(dragEndPoint.y, 0, 0), new Vector3(dragStartPoint.y, 0, 0));

            selectBoxCollider.size = new Vector3(selectbox_x, selectbox_y, 1.0f);

            Vector2 quadrant = dragEndPoint - dragStartPoint;
            quadrant.x = quadrant.x > 0 ? 1 : -1;
            quadrant.y = quadrant.y > 0 ? 1 : -1;

            selectBoxCollider.center = new Vector3(quadrant.x * selectbox_x / 2, quadrant.y * selectbox_y / 2, 0);

            // 全部加入後在一次呼叫不然會收不回被掃到過的音符 // 改了NoteBoxSelect之後就不用了
            MyOnMouseClick?.Invoke(this, new NoteEditorEventArgs(selectedNotelist)); // 現在這樣會跟ontrigger exit互搶


        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            // 結束一次選取時關閉OnTriggerExit跟OntriggerEnter
            selectBoxCollider.gameObject.GetComponent<NoteEditorSelectBox>().boxSelect_ExitDEL = null;
            selectBoxCollider.gameObject.GetComponent<NoteEditorSelectBox>().boxSelect_EnterDEL = null;

            dragEndPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (int i = 0; i < dragLineRenderer.GetComponent<LineRenderer>().positionCount; i++)
            {
                dragLineRenderer.GetComponent<LineRenderer>().SetPosition(i, new Vector2(0, 0));
            }
            selectBoxCollider.size = new Vector3(0, 0, 0);

            
        }
    }


    public void Check_If_Selecting() // 拖動物件的function在這裡
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        
        if (Input.GetKeyDown(KeyCode.Mouse0)) // 因為現在是不管甚麼狀況 只要左鍵沒按到東西就會清空notelist 變成不太好控制 
        {
            // 這裡會把射線打到的gameobject當成參數發通知 接收通知的人判斷傳入的gameobject是不是他自己 
            // 另外如果沒打到東西一樣會傳通知 但是傳入的是隨便一個不是note的物件 也就是大家都不會被選取

            if (Physics.Raycast(ray, out hit) && hit.transform.gameObject.GetComponent<NoteEditorNote>() != null)
            {
                //Debug.Log(hit.transform.gameObject.layer);
                if (selectedNotelist.Count <= 1)
                    selectedNotelist.Clear(); // 如果不加這行就是單點複選 之後功能可以參考 // 可以把它寫成委派 按住shift的時候 = 這個註解掉的功能(但現在只能兩個不知道為什麼
                selectedNotelist.Add(hit.transform.gameObject);

                // 判斷開始boxselect的委派 = null ?
                boxSelectDEL = null;
            }
            
            // 這行是給音樂時間標記的 因為這function最後會呼叫  MyOnMouseClick?.Invoke(this, new NoteEditorEventArgs(selectedNotelist)); 所以把他也加進selectednotelist就可以一起動
            else if (Physics.Raycast(ray, out hit) && hit.transform.gameObject.GetComponent<NoteEditorAudioIndicator>() != null)
            {
                if (selectedNotelist.Count <= 1)
                    selectedNotelist.Clear();
               // selectedNotelist.Add(hit.transform.gameObject);

                boxSelectDEL = null;
            }
            
            else 
            {
                PointerEventData cursor = new PointerEventData(EventSystem.current); // 用graphic raycaster檢測有沒有點到UI 有就不清空select list
                cursor.position = Input.mousePosition;
                List<RaycastResult> objectsHit = new List<RaycastResult>();
                EventSystem.current.RaycastAll(cursor, objectsHit);
                if (objectsHit.Count != 0)
                {
                    Debug.Log("點到UI");
                    return;
                }

                else
                {
                    //
                    selectedNotelist.Clear();

                    // 判斷開始boxselect的委派 = 實作?
                    boxSelectDEL = BoxSelect;
                }
            }
            MyOnMouseClick?.Invoke(this, new NoteEditorEventArgs(selectedNotelist));
            
        }

    }

    public void MoveAllSelectedNote(object sender, NoteEditorEventArgs e)
    {

        for(int i = 0; i < selectedNotelist.Count; i++)
        {
            Vector2 temp = new Vector2(selectedNotelist[i].GetComponent<NoteEditorNote>().lastMoveEndPosx, selectedNotelist[i].GetComponent<NoteEditorNote>().lastMoveEndPosy)
                - e.noteEditorNote.TotalMovedOnLastDrag;

            selectedNotelist[i].transform.position = (Vector3)temp;
   
        }
    }

    public void EndMoveAllSelectedNote(object sender, NoteEditorEventArgs e)
    {
        for (int i = 0; i < selectedNotelist.Count; i++)
        { 
            selectedNotelist[i].GetComponent<NoteEditorNote>().lastMoveEndPosx = selectedNotelist[i].transform.position.x;
            selectedNotelist[i].GetComponent<NoteEditorNote>().lastMoveEndPosy = selectedNotelist[i].transform.position.y;
            selectedNotelist[i].GetComponent<NoteEditorNote>().Update_MeasureNum();
        }
    }

    // 修改左下屬性欄時的動作
    public void AAAA(InputField b)
    {
        //Debug.Log("觸發這裡 內容是:" + b.text);
        for(int i = 0; i < selectedNotelist.Count; i++)
        {
            selectedNotelist[i].GetComponent<NoteEditorNote>().Update_Position(9999, int.Parse(b.text), -1);
            selectedNotelist[i].GetComponent<NoteEditorNote>().ResetOriginalPos(); // 如果沒有這行 移動完再點他就會瞬移回去
        }
        
    }

    // 修改左下屬性欄時的動作
    public void BBBB(InputField c)
    {
        //Debug.Log("觸發這裡 內容是:" + c.text);
        for (int i = 0; i < selectedNotelist.Count; i++)
        {
            selectedNotelist[i].GetComponent<NoteEditorNote>().Update_Position(9999, -1, int.Parse(c.text));
            selectedNotelist[i].GetComponent<NoteEditorNote>().ResetOriginalPos();
        }

    }

    // 用來確認沒有重疊或者非正常位置的音符
    public void ValidCheck()
    {
        NoteEditorNote[] allnoteInScene = GameObject.FindObjectsOfType<NoteEditorNote>(); // 先找到所有音符 
        List<NoteEditorNote> noteForCompare = new List<NoteEditorNote>();
        int overlapNum = 0;

        for (int i = 0; i < allnoteInScene.Length; i++)
        {
            bool isOverlap = false;
            // 如果座標有重複 標示重疊為true
            for (int a = 0; a < noteForCompare.Count; a++)
            {
                //Debug.Log(Vector2.Distance(noteForCompare[a].gameObject.transform.position, allnoteInScene[i].gameObject.transform.position) + "兩點距離");
                if (Vector2.Distance(noteForCompare[a].gameObject.transform.position, allnoteInScene[i].gameObject.transform.position) < pixelPerMeasurement)
                {
                    isOverlap = true;
                    noteForCompare[a].Appearence.material.color = noteOverLappingColor;
                }
            }

            // 如果沒有重疊標示 加入compare陣列
            if (isOverlap == false)
            {
                noteForCompare.Add(allnoteInScene[i]);
            }
            else
            {
                overlapNum++;
                allnoteInScene[i].Appearence.material.color = noteOverLappingColor;
                allnoteInScene[i].transform.position = new Vector3(allnoteInScene[i].transform.position.x, allnoteInScene[i].transform.position.y, allnoteInScene[i].transform.position.z - 0.1f);
            }
            //Debug.Log(noteForCompare.Count);
        }
        
        // 找超出範圍的音符
        for(int i = 0; i < allnoteInScene.Length; i++)
        {
            if(allnoteInScene[i].gameObject.transform.position.x < 0)
            {
                allnoteInScene[i].Appearence.material.color = noteInvalidPosColor;
            }
            if(allnoteInScene[i].gameObject.transform.position.y > rowStartYPos)
            {
                allnoteInScene[i].Appearence.material.color = noteInvalidPosColor;
            }
            
         
        }
        
        
        

        Debug.Log("檢查重疊完成");
        Debug.Log("找到" + overlapNum + "個音符重疊");

    }

    // 調整不對的x軸位置
    public void AdjustNotes()
    {
        NoteEditorNote[] allnoteInScene = GameObject.FindObjectsOfType<NoteEditorNote>(); // 先找到所有音符
        // 調整不對的x軸位置
        for (int i = 0; i < allnoteInScene.Length; i++)
        {

            int adjustedX = (int)(allnoteInScene[i].gameObject.transform.position.x / pixelPerMeasurement);
            //Debug.Log(adjustedX);
            if (allnoteInScene[i].gameObject.transform.position.x % pixelPerMeasurement >= pixelPerMeasurement / 2.0f)
                adjustedX++;
            float adjustedXFloat = adjustedX * pixelPerMeasurement;

            allnoteInScene[i].gameObject.transform.position = new Vector3(adjustedXFloat, allnoteInScene[i].gameObject.transform.position.y, allnoteInScene[i].gameObject.transform.position.z);
            allnoteInScene[i].ResetOriginalPos();
            allnoteInScene[i].SetRealPosition(adjustedXFloat, allnoteInScene[i].gameObject.transform.position.y);
            allnoteInScene[i].Update_MeasureNum();
        }

        Debug.Log("微調音符位置完成");
    }


    // 複製
    public void Copy()
    {
        AdjustNotes();

        if (selectedNotelist.Count <= 0)
            return;
        allnotesinCopyUse.Clear();
        //allnotesinCopyUse = selectedNotelist;//直接等於好像會把記憶體位置搞得一樣 要add才行
        foreach(GameObject g in selectedNotelist)
        {
            allnotesinCopyUse.Add(g);
        }

        copyNotesVector.Clear();
        /*
        List<GameObject> allnotesinCopyUse = selectedNotelist;
        List<Vector3> copyNotesVector = new List<Vector3>(); // 考量到noteeditornote也會帶資訊也得一併複製 可能不能單純存位置而已q
        */
        for (int i = 0; i < allnotesinCopyUse.Count; i++)
        {

            for (int a = i+1; a < allnotesinCopyUse.Count; a++)
            {
                if (allnotesinCopyUse[i].transform.position.x > allnotesinCopyUse[a].transform.position.x)
                {
                    GameObject temp = allnotesinCopyUse[i];
                    allnotesinCopyUse[i] = allnotesinCopyUse[a];
                    allnotesinCopyUse[a] = temp;

                }
            }

        }
        //mostLeftTemp = allnotesinCopyUse[0].GetComponent<NoteEditorNote>();
      
        for (int i = 0; i < allnotesinCopyUse.Count; i++)
        {
            // 現在取出最左邊的了 接著要把相對位置處理進去一個vector3的陣列 
            Vector3 mostLeftvec = allnotesinCopyUse[0].transform.position;
            Vector3 agentvec = allnotesinCopyUse[i].transform.position;
            Vector3 temp = new Vector3((agentvec.x - mostLeftvec.x), agentvec.y, agentvec.z); // 第一個因為自身x相減的關係 都會是0 正好交由之後ctrl v決定他位置
            copyNotesVector.Add(temp); 
            //Debug.Log(allnotesinCopyUse[i].transform.position.x);

        }

        foreach(Vector3 v in copyNotesVector)
        {
           Debug.LogFormat(" x:{0}, y:{1}, z:{2}", v.x, v.y, v.z);
        }
    }

    public void Paste()
    {
        float pasteStartx = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;// 沒有處理起始點的clamp問題
        // 定義拖拉的邊界 // 這邊從note身上鈔的 應該要有一個通用的funcriotn
        pasteStartx = pixelPerMeasurement * (int)(pasteStartx / pixelPerMeasurement);
        pasteStartx = Mathf.Clamp(pasteStartx, 0, measureNum * measureSpacing - pixelPerMeasurement); // pixelPerMeasurement是一小格的距離

        for (int i = 0; i < allnotesinCopyUse.Count; i++)
        {
            Instantiate(allnotesinCopyUse[i], new Vector3(pasteStartx + copyNotesVector[i].x, copyNotesVector[i].y, copyNotesVector[i].z), transform.rotation);
        }

        // 關閉輔助線
        copylineRenderer.SetPosition(0, new Vector3(0, 0, 0));
        copylineRenderer.SetPosition(1, new Vector3(0, 0, 0));
    }

    public void CreateLaserGuideLine()
    {
        Vector3 linepoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Debug.Log("NIIIII");
        copylineRenderer.SetPosition(0, new Vector3(linepoint.x, linepoint.y, 0));
        copylineRenderer.SetPosition(1, new Vector3(linepoint.x, 0, 0));
    }

    int previewNoteCount = 0;
    public void TogglePreview()
    {
        
        if (previewToggle.isOn && indicatorCanStartGoing)
        {
            
            
                
            musicIndicator.transform.position = new Vector3((nowEditSong.time - indicatorDelay) * (BPM / 60.0f) * (measureSpacing / 4.0f) , 0, 0);
            Camera.main.transform.position = new Vector3(musicIndicator.transform.position.x + 9.85f, Camera.main.transform.position.y, Camera.main.transform.position.z);

            if (previewNoteCount < FindnoteList.Count && musicIndicator.transform.position.x >= FindnoteList[previewNoteCount].transform.position.x)
            {
                Instantiate(preveiewHitEffect, FindnoteList[previewNoteCount].transform.position, transform.rotation);
                previewNoteCount++;
                //Debug.Log("AA");
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                musicIndicator.transform.position = new Vector3(Camera.main.transform.position.x, 0, 0);
            }
        }
    }

   
    List<GameObject> FindnoteList;
    public void SetPlayingSong ()
    {
        FindnoteList = new List<GameObject>();
        FindnoteList.Clear();
        if (previewToggle.isOn)
        {
            GameObject[] FindNotes = GameObject.FindGameObjectsWithTag("notes");
            foreach (GameObject g in FindNotes)
            {
              
                //Debug.Log(g.transform.position.x + "--wtf");
            }
            previewNoteCount = 0;
            
            // 排序
            for (int i = 0; i < FindNotes.Length - 1; i++)
            {
                for (int a = i + 1; a < FindNotes.Length; a++)
                {
                    //NoteEditorNote temp = FindNotes[i].GetComponent<NoteEditorNote>(); // 有跟output script的小改一下 這邊用絕對位置就好
                    float position = FindNotes[i].transform.position.x;
                    //NoteEditorNote temp_next = FindNotes[i + 1].GetComponent<NoteEditorNote>();
                    float position_next = FindNotes[a].transform.position.x;

                    if (position > position_next)
                    {
                        GameObject temp_swap = FindNotes[i];
                        FindNotes[i] = FindNotes[a];
                        FindNotes[a] = temp_swap;
                    }
                }
            }
            
            
            setDelayTimeTemp = (musicIndicator.transform.position.x / (BPM / 60.0f) / (measureSpacing / 4.0f)) /*- startDelay*/;
            Debug.Log(setDelayTimeTemp);
           
            nowEditSong.Play();
            nowEditSong.time = startDelay + setDelayTimeTemp;
            indicatorCanStartGoing = true;
            indicatorDelay = startDelay;
            //StartCoroutine(WaitAndSetIndicator());

            
            
            //nowEditSong.time = setDelayTimeTemp;

            foreach (GameObject g in FindNotes)
            {
                if (g.transform.position.x >= musicIndicator.transform.position.x)
                    FindnoteList.Add(g); ;

                
                //Debug.Log(g.transform.position.x);
            }

        }
        else
        {
            nowEditSong.Stop();
            indicatorCanStartGoing = false;
            indicatorDelay = 0;
        }
    }
    private IEnumerator WaitAndSetIndicator(float waitTime)
    {

        yield return new WaitForSeconds(waitTime);
        indicatorCanStartGoing = true;



    }

}
