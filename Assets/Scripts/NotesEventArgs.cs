﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// 這裡用來設定生成音符時要做的事情
public class NotesEventArgs : System.EventArgs
{

    // 現在有個問題是 合併後的eventargs 如果有參數沒set到又呼叫 會有null
    public bool[] spawners;
    public NoteAttributes theNoteOnthisSquare;

    public float tolerence , nextNoteTiming;

    public NotesEventArgs(NoteAttributes _NoteAttribute, bool[] _spawners)
    {
        spawners = _spawners;
    }

    public NotesEventArgs(float _tolerence, bool[] _spawners, float _nextNoteTiming)
    {
        
        tolerence = _tolerence;
        spawners = _spawners;
        nextNoteTiming = _nextNoteTiming;
    }

    // for laser
    public float startPoint;
    public int swipeDir;
    public float swipeSpeed;
    public float periodInMeasure32;
    public float warningTimeInMeasure32;

    public NotesEventArgs(float _startPoint, int _swipeDir, float _swipeSpeed, float _periodInMeasure32, float _warningTimeInMeasure32)
    {
        startPoint = _startPoint;
        swipeDir = _swipeDir;
        swipeSpeed = _swipeSpeed;
        periodInMeasure32 = _periodInMeasure32;
        warningTimeInMeasure32 = _warningTimeInMeasure32;

    }
}
