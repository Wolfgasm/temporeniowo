﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class NoteReceiver : MonoBehaviour
{

    // 目前判斷能調整的空間蠻小的 可能要針對個別的音符來啟動而不是接收器
    Metronome _metronome;
    public int receiverNumber; // 現在太分散 最好集中管理
    public static string[] primTriggersName  = new string[2] {"RightPrimTrigger", "LeftPrimTrigger" };
    public static string[] wheelTriggersName = new string[2] { "R_HitTriiger", "L_HitTriiger" };
    public Transform[] sparkPos;
    public Transform[] frontSparkPos;
    public Transform[] cdPos;
    public Transform[] energyPos;
    public GameObject sparkInstance;
    public GameObject frontSparkInstance;
    public GameObject cdInstance;
    public GameObject energyInstance;

    // TEST
    public Text b;


    public Slider slider;

    public Text combo;

    bool threshHold = false;
    float threshHoldTimer = 0;
    int spawner;


    public string[] playingButtons; // 這裡最好不要這樣寫會變成每一個都要設定

    public delegate void CallPressContainer();

    public CallPressContainer callPressContainer;

    bool unMiss = true;
    float tolerenceTemp;

    public GameObject _inputReactingBar;
    public Color normalColor;
    public Color goodHitColor;
    public Color badHitColor;
    private Color nowColor;
    Material _reactingBarMaterial;

    public Animator planeAnimator;

    public Image tutorialImage;
    void Awake()
    {
        _metronome = GameObject.FindGameObjectWithTag("GameController").GetComponent<Metronome>();
        this._metronome.TimeToPress += this.NoteThreshHoldOn;
        _reactingBarMaterial = _inputReactingBar.GetComponent<Renderer>().material;

        cdPos = frontSparkPos;
        //energyPos = sparkPos;
    }

    void Update()
    {
        // 撞針動畫
        if (Input.GetButtonDown(playingButtons[receiverNumber]))
        {
            // 撞針動畫
            planeAnimator.SetTrigger(primTriggersName[receiverNumber]);
        }


        if (threshHoldTimer > 0)
        {
            threshHoldTimer -= Time.deltaTime;
        }
        else if (threshHoldTimer <= 0)
        {
            //Debug.Log(unMiss);
            threshHoldTimer = 0;
            threshHold = false;
            callPressContainer = DisCombo;
            if (unMiss == false)
            {
                DisCombo();
                Metronome.combo = 0;
                Metronome.miss++;
                unMiss = true;
            }

        }
        slider.value = threshHoldTimer;
        b.text = threshHold == true ? "★" : "☆";

        callPressContainer?.Invoke();
        nowColor = Vector4.Lerp(nowColor, normalColor, Time.deltaTime * 5.0f);

        _reactingBarMaterial.SetColor("_EmissiveColor", nowColor);
        //Press();

    }
    void NoteThreshHoldOn(object sender, NotesEventArgs e)
    {
        threshHoldTimer = 0;
        bool foundPressing = false;
        
        //Debug.Log(e.nextNoteTiming + "<< nextnoteTiming" );
        if (e.nextNoteTiming > 0 && e.nextNoteTiming <= e.tolerence)
        {
            threshHoldTimer = e.nextNoteTiming / 1.5f;
            //threshHoldTimer = 0;
            //Debug.Log(threshHoldTimer + "threshHolde");
        }
        else
        {
            threshHoldTimer = e.tolerence * 2.0f;
        }
        //threshHoldTimer = e.tolerence * 2.0f;
        tolerenceTemp = threshHoldTimer;
        if (tolerenceTemp <= 0.1f)
            tolerenceTemp = 0.15f;
        //Debug.Log(threshHoldTimer + "threshHolde");
        for (int i = 0; i < e.spawners.Length; i++)
        {
            if (e.spawners[receiverNumber] == true) // 應該要偵測到就不再回圈 不然會永遠都是true或是永遠是false
            {
                this.threshHold = true;
                callPressContainer = Press;
                unMiss = false;
                foundPressing = true;
                
            }
            else
            {

                unMiss = true;
            }
            if(foundPressing == true)
            {
                break;
            }
        }

       

    }

    public static int good, bad = 0;
    void Press()
    {
        

        if (threshHold == true && Input.GetButtonDown(playingButtons[receiverNumber]))
        {

            if (threshHoldTimer >= tolerenceTemp / 8.0f && threshHoldTimer <= (tolerenceTemp / 8.0f * 6f))
            {
                //Debug.Log("GOOD");
                //_reactingBarMaterial.SetColor("_EmissiveColor", new Vector4(goodHitColor.r * 1, goodHitColor.r * 1, goodHitColor.r, 1.0f));
                nowColor = goodHitColor * 5.0f;
                good++;
                Metronome.good++;
                GetComponent<DJPADS>().RegeneratePadsColor();

                // 彈夾動畫
                planeAnimator.SetTrigger(wheelTriggersName[receiverNumber]);

                // 彈夾火花
                GameObject cloneObject = Instantiate(sparkInstance);
                cloneObject.transform.parent = sparkPos[receiverNumber].transform;
                cloneObject.transform.localPosition = new Vector3(0, 0, 0);

                // 槍口
                GameObject cloneObjectB = Instantiate(frontSparkInstance);
                cloneObjectB.transform.parent = frontSparkPos[receiverNumber].transform;
                cloneObjectB.transform.localPosition = new Vector3(0, 0, 0);

                // cd發射
                GameObject cloneObjectC = Instantiate(cdInstance);
                Random ran = new Random();
                cloneObjectC.transform.parent = cdPos[receiverNumber].transform;
                cloneObjectC.transform.rotation = cdPos[receiverNumber].transform.rotation;
                cloneObjectC.transform.localPosition = new Vector3(0, 0, 0);
                cloneObjectC.transform.Rotate(new Vector3(Random.Range(-30f, 30.0f), 0, Random.Range(-10f, 10.0f)));

                // 蒐集能量
                GameObject cloneObjectD = Instantiate(energyInstance);
                cloneObjectD.transform.parent = energyPos[receiverNumber].parent;
                //cloneObjectD.transform.localPosition = new Vector3(0, 0, 0);
                cloneObjectD.transform.localPosition = energyPos[receiverNumber].transform.position;
                //Debug.Log(GameObject.FindGameObjectWithTag("EnergyCollectR").name);
                cloneObjectD.transform.Rotate(new Vector3(Random.Range(-30f, 30.0f), 0, Random.Range(-10f, 10.0f)));
                tutorialImage.gameObject.SetActive(false);
            }
            else
            {
                //Debug.Log("BAD");
                //_reactingBarMaterial.SetColor("_EmissiveColor", new Vector4(badHitColor.r * 1, badHitColor.r * 1, badHitColor.r, 1.0f));
                nowColor = badHitColor * 2.0f;
                bad++;
                Metronome.bad++;
                GetComponent<DJPADS>().RegeneratePadsColor();
                // 彈夾動畫
                planeAnimator.SetTrigger(wheelTriggersName[receiverNumber]);

                // 彈夾火花
                GameObject cloneObject = Instantiate(sparkInstance);
                cloneObject.transform.parent = sparkPos[receiverNumber].transform;
                cloneObject.transform.localPosition = new Vector3(0, 0, 0);

                // 槍口
                GameObject cloneObjectB = Instantiate(frontSparkInstance);
                cloneObjectB.transform.parent = frontSparkPos[receiverNumber].transform;
                cloneObjectB.transform.localPosition = new Vector3(0, 0, 0);

                
                // cd發射
                GameObject cloneObjectC = Instantiate(cdInstance);
                Random ran = new Random();
                cloneObjectC.transform.parent = cdPos[receiverNumber].transform;
                cloneObjectC.transform.rotation = cdPos[receiverNumber].transform.rotation;
                cloneObjectC.transform.localPosition = new Vector3(0, 0, 0);
                cloneObjectC.transform.Rotate(new Vector3(Random.Range(-30f, 30.0f), 0, Random.Range(-10f, 10.0f)));

                // 蒐集能量
                GameObject cloneObjectD = Instantiate(energyInstance);
                cloneObjectD.transform.parent = energyPos[receiverNumber].parent;
                //cloneObjectD.transform.localPosition = new Vector3(0, 0, 0);
                cloneObjectD.transform.localPosition = energyPos[receiverNumber].transform.position;
                cloneObjectD.transform.Rotate(new Vector3(Random.Range(-30f, 30.0f), 0, Random.Range(-10f, 10.0f)));

                tutorialImage.gameObject.SetActive(false);
            }
            Metronome.score++; // 以後也用事件可能比較好
            Metronome.combo += 1;
            Metronome.nowUltimateNum += Metronome.each_note_Ultimate_count;
            callPressContainer = DisCombo; // 按過就關掉了
            combo.text = "Combo : " + Metronome.combo + " total Good : " + good + " BAD : " + bad;
            //Debug.Log(Metronome.score);
            //_reactingBarMaterial.SetColor("_EmissiveColor", new Vector4(Color.red.r * 2, Color.red.g * 2, Color.red.b, 2.0f));
            unMiss = true;
        }

    }
    void DisCombo()
    {
        if (threshHold == false && Input.GetButtonDown(playingButtons[receiverNumber]))
        {
            Metronome.combo = 0;
            combo.text = "Combo : " + Metronome.combo + " total Good : " + good + " BAD : " + bad;
            Metronome.nowUltimateNum -= Metronome.each_note_Ultimate_count * 2.0f;
        }
       // Metronome.combo = 0;
    }

}




