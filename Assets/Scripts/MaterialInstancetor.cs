﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 此檔案用來將material改成material instance
// 把這行拉到有屬性是_POSITION的material的物件上 然後下面那行反註解
[ExecuteAlways]
public class MaterialInstancetor : MonoBehaviour
{
    Renderer the_renderer;
    // Start is called before the first frame update
    void Start()
    {
        the_renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        the_renderer.material.SetFloat("_POSITION", 1.0f);
    }
}
