﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteInstance : MonoBehaviour
{
    

    private Vector3 thisNotePos;
    public Transform noteBoopTargetPos; // 生成時設定


    private float distanceToBoopTarget;
    public float arriveTime = 0.5f; // 忘記設置會出現 {NaN,NaN, Infinity} 因為除數為0或不存在 // 之後寫回去metronoe裡面

    private Vector3 toBoopTargetDir;  // 生成時設定?
    private float boopSpeed;

    // 測試用時間
    private float liveTimer = 0;
    void Start()
    {
        thisNotePos = transform.position;
        distanceToBoopTarget = Vector3.Distance(noteBoopTargetPos.position, thisNotePos);

        toBoopTargetDir = noteBoopTargetPos.transform.position - thisNotePos;

        
        arriveTime = Metronome.arriveTime;

        // 確保下一行除數不為0
        if (arriveTime == 0)
        {
            arriveTime = 0.0001f;
        }
        boopSpeed = distanceToBoopTarget / arriveTime;

        //Debug.Log(distanceToBoopTarget + "距離" + boopSpeed + "飛行速度");
    }


    // 要在FixedUpdate做時間抵達才會準
    void FixedUpdate()
    {
        liveTimer += Time.deltaTime;    //測試用秒數

        transform.position += toBoopTargetDir.normalized * boopSpeed * Time.deltaTime;

        /*
        // 測試一下是否真的在秒數上抵達
        if (Vector3.Distance(transform.position, noteBoopTargetPos.position) <= 0.05)
        {
            //Debug.Log(liveTimer + "時音符抵達" + boopSpeed);
            
        }*/

        if(liveTimer >= 5.0f)
        {
            //Destroy(this);
        }
    }
}
