﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboCount : MonoBehaviour
{
    public Font displayFont;
    public List<MeshFilter> displayPlaneFileter;

    public int displayingNumber;

    public Material customFontMaterial;

    string str = "Hello World";
    private void Awake()
    {

        /*
        for (int i = 0; i < displayPlaneFileter.Count; i++)
        {
            displayPlaneFileter[i].GetComponent<MeshRenderer>().material.mainTexture = displayFont.material.mainTexture;
        }
        */

        // Set the rebuild callback so that the mesh is regenerated on font changes.

        Font.textureRebuilt += OnFontTextureRebuilt;
        // Request characters.
        displayFont.RequestCharactersInTexture("6");

        // Set up mesh.
        displayPlaneFileter[0].mesh = new Mesh();
        //GetComponent<MeshFilter>().mesh = displayPlaneFileter[0].mesh;
        //GetComponent<MeshRenderer>().material = displayFont.material;
        displayPlaneFileter[0].GetComponent<MeshRenderer>().material = displayFont.material;
        // Generate font mesh.
        RebuildMesh();
    }




    // Update is called once per frame
    void Update()
    {
        // Keep requesting our characters each frame, so Unity will make sure that they stay in the font when regenerating the font texture.
        displayFont.RequestCharactersInTexture("6");
        //CreateFont("6");
        //RebuildMesh("6");
        
    }

    void CreateFont(string _output)
    {
        // Call this function to request Unity to make sure all the characters in the string characters are available in the font's font texture 
        // 上面是unity文件寫的 我不知道為什麼要請unity make sure ==
        displayFont.RequestCharactersInTexture(_output);

        for (int i = 0; i < _output.Length; i++)
        {
            CharacterInfo character;
            // Get rendering info for a specific character. << 也是文件寫的
            // 應該是拿render用的資訊放進上面的character
            displayFont.GetCharacterInfo(_output[i], out character);

            Vector2[] thisCharacterUVs = new Vector2[4];

            thisCharacterUVs[0] = character.uvBottomLeft;
            thisCharacterUVs[1] = character.uvTopRight;
            thisCharacterUVs[2] = character.uvBottomRight;
            thisCharacterUVs[3] = character.uvTopLeft;

            displayPlaneFileter[i].mesh.uv = thisCharacterUVs;
            //Debug.Log(displayPlaneFileter[i].mesh.vertices.Length + "到底多少");

            // why
            Vector3 newScale = displayPlaneFileter[i].transform.localScale;
            newScale.x = character.glyphWidth * 0.02f;

            //displayPlaneFileter[i].transform.localScale = newScale;
        }


    }

    void RebuildMesh()
    {
        // Generate a mesh for the characters we want to print.
        var vertices = new Vector3[str.Length * 4];
        var triangles = new int[str.Length * 6];
        var uv = new Vector2[str.Length * 4];
        Vector3 pos = Vector3.zero;
        for (int i = 0; i < str.Length; i++)
        {
            // Get character rendering information from the font
            CharacterInfo ch;
            displayFont.GetCharacterInfo(str[i], out ch);

            vertices[4 * i + 0] = pos + new Vector3(ch.minX, ch.maxY, 0);
            vertices[4 * i + 1] = pos + new Vector3(ch.maxX, ch.maxY, 0);
            vertices[4 * i + 2] = pos + new Vector3(ch.maxX, ch.minY, 0);
            vertices[4 * i + 3] = pos + new Vector3(ch.minX, ch.minY, 0);

            uv[4 * i + 0] = ch.uvTopLeft;
            uv[4 * i + 1] = ch.uvTopRight;
            uv[4 * i + 2] = ch.uvBottomRight;
            uv[4 * i + 3] = ch.uvBottomLeft;

            triangles[6 * i + 0] = 4 * i + 0;
            triangles[6 * i + 1] = 4 * i + 1;
            triangles[6 * i + 2] = 4 * i + 2;

            triangles[6 * i + 3] = 4 * i + 0;
            triangles[6 * i + 4] = 4 * i + 2;
            triangles[6 * i + 5] = 4 * i + 3;

            // Advance character position
            pos += new Vector3(ch.advance, 0, 0);
        }
       /* displayPlaneFileter[0].mesh.vertices = vertices;
        displayPlaneFileter[0].mesh.triangles = triangles;
        displayPlaneFileter[0].mesh.uv = uv;*/
    }

    void OnFontTextureRebuilt(Font changedFont)
    {
        if (changedFont != displayFont)
            return;

        RebuildMesh();
    }
}
